//This gets called by the makefile to compile all your source code. Add to it as needed!

void makeLibs(TString opt=""){

  //Remove the contents of the binary directory if option "clean" is called
  if (opt == "clean"){
    gSystem->Exec("rm -f bin/*");
    return;
  }

  //Check to make sure the bin directory exists. If not create it.
  if (gSystem->OpenDirectory("bin") == 0)
    gSystem->MakeDirectory("bin");

  //Set the Build and Include Directories
  gSystem->SetBuildDir("$PWD/bin/",true);
  gSystem->SetIncludePath(TString::Format("-I$ROOTSYS/include -I%s/inc",gSystem->pwd()));

  //Compile the contents of the sub-modules
  gSystem->CompileMacro("src/submodules/datacollectorreaderlibs/TrackInfo/TrackInfo.cxx","k");
  gSystem->CompileMacro("src/submodules/datacollectorreaderlibs/PrimaryVertexInfo/PrimaryVertexInfo.cxx","k");
  gSystem->CompileMacro("src/submodules/datacollectorreaderlibs/EventInfo/EventInfo.cxx","k");
  gSystem->CompileMacro("src/submodules/datacollectorreaderlibs/DavisDstReader/DavisDstReader.cxx","k");
  gSystem->CompileMacro("src/submodules/ParticleInfo/ParticleInfo/ParticleInfo.cxx","k");
  gSystem->CompileMacro("src/submodules/strefmultextendedcorr/StRefMultExtendedCorr.cxx","k");
  gSystem->CompileMacro("src/submodules/centralitydatabaseclass/CentralityDatabaseClass/CentralityDatabaseClass.cxx","k");
  gSystem->CompileMacro("src/submodules/SimultaneousFitter/SimFitter.cxx","k");
  
  //Compile your source code
  gSystem->CompileMacro("userfiles/UserCuts.cxx","k");
  gSystem->CompileMacro("src/analysis/utilityFunctions.cxx","k");
  gSystem->CompileMacro("src/analysis/usefulDataStructs.cxx","k");
  gSystem->CompileMacro("src/analysis/readerExample.cxx","k");
  gSystem->CompileMacro("src/analysis/centralityVariableDistributions.cxx","k");
  gSystem->CompileMacro("src/analysis/skimmerAndBinner.cxx","k");
  gSystem->CompileMacro("src/analysis/tofSkimmerBinner.cxx","k");
  gSystem->CompileMacro("src/analysis/fitZTPCUtilities.cxx","k");
  gSystem->CompileMacro("src/analysis/fitZTPCPions.cxx","k");
  gSystem->CompileMacro("src/analysis/YieldExtractionFit.cxx","k");
  gSystem->CompileMacro("src/analysis/generateRawSpectra.cxx","k");
  gSystem->CompileMacro("src/analysis/SpectraCorrectionFunctions.cxx","k");
  gSystem->CompileMacro("src/spectraUtilities/SpectraCorrectionCurves.cxx","k");
  gSystem->CompileMacro("src/spectraUtilities/SpectraClass.cxx","k");
  gSystem->CompileMacro("src/spectraUtilities/SpectraFitFunctions.cxx","k");
  gSystem->CompileMacro("src/spectraUtilities/SpectraFitUtilities.cxx","k");
  gSystem->CompileMacro("src/analysis/correctSpectra.cxx","k");
  gSystem->CompileMacro("src/analysis/combineSpectra.cxx","k");
  //gSystem->CompileMacro("src/analysis/fitSpectra.cxx","k");
  gSystem->CompileMacro("src/spectraFitting/fitPionSpectra.cxx","k");
  gSystem->CompileMacro("src/spectraFitting/fitProtonSpectra.cxx","k");
  gSystem->CompileMacro("src/spectraFitting/fitKaonSpectra.cxx","k");
  //gSystem->CompileMacro("src/analysis/fitProtonSpectra.cxx","k");
  gSystem->CompileMacro("src/analysis/simultaneousBlastWaveFit.cxx","k");
  gSystem->CompileMacro("src/analysis/generateRapidityDensityDistributions.cxx","k");
  
  //InterBin mTm0 Study
  gSystem->CompileMacro("src/interBinmTm0Study/MakeInterBinmTm0Distributions.cxx","k");
  gSystem->CompileMacro("src/interBinmTm0Study/DoInterBinmTm0Distributions.cxx","k");
  
  //PID Calibrator
  gSystem->CompileMacro("src/pidCalibration/BindEdxInRapidityMtM0.cxx","k");
  gSystem->CompileMacro("src/pidCalibration/FitdEdxBetaGamma.cxx","k");
  gSystem->CompileMacro("src/pidCalibration/ExtractMeansAndWidths.cxx","k");
  
  //TOF Matching Efficiency
  gSystem->CompileMacro("src/tofMatchingEfficiency/MakeTofMatchEfficiencyHistos.cxx","k");
  gSystem->CompileMacro("src/tofMatchingEfficiency/MakeTofMatchEfficiencyGraphs.cxx","k");
  gSystem->CompileMacro("src/tofMatchingEfficiency/FitTofMatchEfficiencyGraphs.cxx","k");
  
  //Quality Assurance Plots
  gSystem->CompileMacro("src/qa/eventQAmaker.cxx","k");
  gSystem->CompileMacro("src/qa/vertexQAmaker.cxx","k");
  gSystem->CompileMacro("src/qa/trackQAmaker.cxx","k");
  gSystem->CompileMacro("src/qa/toftrackQAmaker.cxx","k");
	gSystem->CompileMacro("src/qa/pidQAmaker.cxx","k");
  gSystem->CompileMacro("src/qa/RunByRunDetectorPerformance.cxx","k");
  gSystem->CompileMacro("src/qa/IdentifyBadRunsEtaPhi.cxx","k");
  gSystem->CompileMacro("src/qa/ColliderQA.cxx","k");
  gSystem->CompileMacro("src/qa/ColliderQADocumentGenerator.cxx","k");
  
  //Embedding Related Code
	gSystem->CompileMacro("src/embedding/createCentralityDatabaseCSV.cxx","k");
  gSystem->CompileMacro("src/embedding/EfficiencyFitUtilities.cxx","k");
  gSystem->CompileMacro("src/embedding/GenerateCentralityDatabase.cxx","k");
  gSystem->CompileMacro("src/embedding/MakeCorrectionCurves.cxx","k");
  gSystem->CompileMacro("src/embedding/CreateEfficiencyTable.cxx","k");

  //Background
  gSystem->CompileMacro("src/background/knockoutProtons.cxx","k");
  gSystem->CompileMacro("src/background/knockoutProtonAnalysis.cxx","k");
  gSystem->CompileMacro("src/background/muonContamination.cxx","k");
  gSystem->CompileMacro("src/background/feeddownBackground.cxx","k");

    
  //If your code requires any dependencies from the glaubermcmodel submodule
  //make them under the following line
  gSystem->Load("bin/GlauberClass_cxx.so");
  
	//Pile up
	gSystem->CompileMacro("src/analysis/pileupChiSquaredMinimization.cxx","k");
	gSystem->CompileMacro("src/analysis/pileupLuminosity.cxx","k");
  
}
