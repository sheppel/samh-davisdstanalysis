#include "../inc/globalDefinitions.h"
#include "../inc/StyleSettings.h"

void ScaleByNpart(TGraphErrors *gr){

  for (int i=0; i<gr->GetN(); i++){
    double nPart = gr->GetX()[i];
    double nPartErr = gr->GetEX()[i];
    double yield = gr->GetY()[i];
    double yieldErr = gr->GetEY()[i];

    double ratio = yield / nPart;
    double ratioErr =ratio * sqrt(pow(yieldErr/yield,2) + pow(nPartErr/nPart,2)); 
    
    gr->SetPoint(i,nPart,ratio);
    gr->SetPointError(i,nPartErr,ratioErr);
  }
  
}

void DrawMidRapiditydNdyComparison(TString eventConfig, TString system, Int_t speciesIndex,
				   Int_t charge, bool scaleByNpart=false){

  const double midY(0.0);
  const int nEnergies(7);
  const int nCentBins = 9;
  Double_t energies[nEnergies] = {7.7,11.5,14.5,19.6,27.0,39.0,62.4};
  
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/SpectraFitFunctions_cxx.so");
  SetVariableUserCuts(7.7,eventConfig,"");
  ParticleInfo *particleInfo = new ParticleInfo();

  //Result Files
  TFile *spectraFile[nEnergies];
  spectraFile[0] = new TFile(Form("../userfiles/AuAu07/outputFiles/AuAu07_%s_Results.root",eventConfig.Data()),"READ");
  spectraFile[1] = new TFile(Form("../userfiles/AuAu11/outputFiles/AuAu11_%s_Results.root",eventConfig.Data()),"READ");
  spectraFile[2] = new TFile(Form("../userfiles/AuAu14/outputFiles/AuAu14_%s_Results.root",eventConfig.Data()),"READ");
  spectraFile[3] = new TFile(Form("../userfiles/AuAu19/outputFiles/AuAu19_%s_Results.root",eventConfig.Data()),"READ");
  spectraFile[4] = new TFile(Form("../userfiles/AuAu27/outputFiles/AuAu27_%s_Results.root",eventConfig.Data()),"READ");
  spectraFile[5] = new TFile(Form("../userfiles/AuAu39/outputFiles/AuAu39_%s_Results.root",eventConfig.Data()),"READ");
  spectraFile[6] = new TFile(Form("../userfiles/AuAu62/outputFiles/AuAu62_%s_Results.root",eventConfig.Data()),"READ");

  //Create the Spectra Name
  TString type = "corrected";
  TString Type = "Corrected";
  TString name = "Corrected";
  
  //Get the dNdy Graphs
  TGraphErrors *dNdyGraph[nEnergies][nCentBins];
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
      dNdyGraph[iEnergy][iCentBin] = (TGraphErrors *)spectraFile[iEnergy]->
	Get(Form("RapidityDensity_%s/RapidityDensity_%s_Cent%02d_Symm",
		 particleInfo->GetParticleName(speciesIndex,charge).Data(),
		 particleInfo->GetParticleName(speciesIndex,charge).Data(),
		 iCentBin));
    }
  }

  //Create the Mid Rapidity dNdy Graphs for each energy
  TGraphErrors *midYdNdyGraph[nEnergies];
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){

    midYdNdyGraph[iEnergy] = new TGraphErrors();
    midYdNdyGraph[iEnergy]->SetMarkerStyle(kFullCircle);
    midYdNdyGraph[iEnergy]->SetMarkerColor(GetEnergyColor(energies[iEnergy]));
    for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){

      int midYPointIndex = TGraphFindPoint((TGraph *)dNdyGraph[iEnergy][iCentBin],midY);
      std::pair<double,double> nPart = GetAverageNpart(energies[iEnergy],iCentBin);

      midYdNdyGraph[iEnergy]->SetPoint(iCentBin,nPart.first,dNdyGraph[iEnergy][iCentBin]->GetY()[midYPointIndex]);
      midYdNdyGraph[iEnergy]->SetPointError(iCentBin,nPart.second,dNdyGraph[iEnergy][iCentBin]->GetEY()[midYPointIndex]);
    }
    
  }
  
  //Get the Previous STAR Results
  TGraphErrors *star_prev[nEnergies];
  star_prev[0] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu07.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));
  star_prev[1] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu11.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));
  star_prev[2] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu14.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));
  star_prev[3] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu19.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));
  star_prev[4] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu27.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));
  star_prev[5] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu39.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));
  star_prev[6] = new TGraphErrors(Form("STAR/STAR_MidY_%s_AuAu62.txt",particleInfo->GetParticleName(speciesIndex,charge).Data()));

  if (scaleByNpart){
    for (int i=0; i<nEnergies; i++){
      ScaleByNpart(star_prev[i]);
      ScaleByNpart(midYdNdyGraph[i]);
    }
    
  }

  for (int i=0; i<nEnergies; i++){
    star_prev[i]->SetMarkerStyle(kFullSquare);
    star_prev[i]->SetMarkerColor(GetEnergyColor(energies[i]));
  }

  //*******
  // dNdy As A Function Of NPart
  //*******
  TCanvas *dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,20,800,600);
  dNdyCanvas->SetTopMargin(0.05);
  dNdyCanvas->SetRightMargin(.05);
  dNdyCanvas->SetTicks();
  
  TH1F *frame = dNdyCanvas->DrawFrame(0,0,365,scaleByNpart ? 0.35:65);
  frame->GetXaxis()->SetTitle("<N_{part}>");
  if (scaleByNpart)
    frame->GetYaxis()->SetTitle("(dN/dy)|_{y=0} / <N_{part}>");
  else
    frame->GetYaxis()->SetTitle("(dN/dy)|_{y=0}");
  
  frame->GetXaxis()->SetTitleFont(63);
  frame->GetXaxis()->SetTitleSize(28);
  frame->GetYaxis()->SetTitleFont(63);
  frame->GetYaxis()->SetTitleSize(28);
  frame->GetXaxis()->SetTitleOffset(0.9);
  frame->GetYaxis()->SetTitleOffset(0.80);

  for (int i=0; i<nEnergies; i++){
    star_prev[i]->Draw("PZ");
    midYdNdyGraph[i]->Draw("PZ");
  }

  TPaveText *dNdyTitle = new TPaveText(.2,.84,.58,.91,"NDC");
  dNdyTitle->SetBorderSize(0);
  dNdyTitle->SetFillColor(kWhite);
  dNdyTitle->SetTextFont(63);
  dNdyTitle->SetTextSize(30);
  dNdyTitle->AddText(Form("%s | %s dN/dy at Mid-Rapidity",
			  system.Data(),particleInfo->GetParticleSymbol(speciesIndex,charge).Data()));
  dNdyTitle->Draw("SAME");

  TLegend *dNdyLeg = new TLegend(.11,.54,.48,.83);
  dNdyLeg->SetBorderSize(0);
  dNdyLeg->SetFillColor(kWhite);
  dNdyLeg->SetFillStyle(0);
  dNdyLeg->SetTextFont(63);
  dNdyLeg->SetTextSize(22);
  dNdyLeg->SetNColumns(3);
  dNdyLeg->AddEntry((TObject *)NULL,"7.7","");
  dNdyLeg->AddEntry(midYdNdyGraph[0],"This","P");
  dNdyLeg->AddEntry(star_prev[0],"Previous","P");
  dNdyLeg->AddEntry((TObject *)NULL,"11.5","");
  dNdyLeg->AddEntry(midYdNdyGraph[1],"This","P");
  dNdyLeg->AddEntry(star_prev[1],"Previous","P");
  dNdyLeg->AddEntry((TObject *)NULL,"14.5","");
  dNdyLeg->AddEntry(midYdNdyGraph[2],"This","P");
  dNdyLeg->AddEntry((TObject *)NULL,"Previous","");
  dNdyLeg->AddEntry((TObject *)NULL,"19.6","");
  dNdyLeg->AddEntry(midYdNdyGraph[3],"This","P");
  dNdyLeg->AddEntry(star_prev[3],"Previous","P");
  dNdyLeg->AddEntry((TObject *)NULL,"27.0","");
  dNdyLeg->AddEntry(midYdNdyGraph[4],"This","P");
  dNdyLeg->AddEntry(star_prev[4],"Previous","P");
  dNdyLeg->AddEntry((TObject *)NULL,"39.0","");
  dNdyLeg->AddEntry(midYdNdyGraph[5],"This","P");
  dNdyLeg->AddEntry(star_prev[5],"Previous","P");
  dNdyLeg->AddEntry((TObject *)NULL,"62.4","");
  dNdyLeg->AddEntry(midYdNdyGraph[6],"This","P");
  dNdyLeg->AddEntry(star_prev[6],"Previous","P");
  dNdyLeg->Draw("SAME");

  //*******
  //Ratio of Previous Measurements to Present Measurements
  //******

  //Make the ratio graphs
  TGraphErrors *ratioGraph[nEnergies];
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    
    ratioGraph[iEnergy] = new TGraphErrors();
    ratioGraph[iEnergy]->SetMarkerColor(GetEnergyColor(energies[iEnergy]));
    ratioGraph[iEnergy]->SetMarkerStyle(kFullCircle);

    //Skip 14 For now
    if (iEnergy == 2)
      continue;
    
    for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){

      std::pair<double,double> nPart = GetAverageNpart(energies[iEnergy],iCentBin);
      int prevPointIndex = TGraphFindPoint(star_prev[iEnergy],nPart.first);
      int presPointIndex = TGraphFindPoint(midYdNdyGraph[iEnergy],nPart.first);
      
      Double_t val1 = star_prev[iEnergy]->GetY()[prevPointIndex];
      Double_t val1Err = star_prev[iEnergy]->GetEY()[prevPointIndex];
      Double_t val2 = midYdNdyGraph[iEnergy]->GetY()[presPointIndex];
      Double_t val2Err = midYdNdyGraph[iEnergy]->GetEY()[presPointIndex];
      
      Double_t ratio =  val1 / val2;
      Double_t ratioErr = ratio * sqrt(pow(val1Err/val1,2) + pow(val2Err/val2,2));

      ratioGraph[iEnergy]->SetPoint(iCentBin,nPart.first,ratio);
      ratioGraph[iEnergy]->SetPointError(iCentBin,nPart.second,ratioErr);
      
    }
  }
  
  TCanvas *ratioCanvas = new TCanvas("ratioCanvas","ratioCanvas",850,20,800,600);
  ratioCanvas->SetTicks();
  ratioCanvas->SetTopMargin(.05);
  ratioCanvas->SetRightMargin(.05);

  TH1F *ratioFrame = ratioCanvas->DrawFrame(0,.4,365,1.5);
  ratioFrame->GetYaxis()->SetTitle("Present Analysis/ Previous Analysis");
  ratioFrame->GetXaxis()->SetTitle("<N_{part}>");
  ratioFrame->GetXaxis()->SetTitleFont(63);
  ratioFrame->GetXaxis()->SetTitleSize(28);
  ratioFrame->GetYaxis()->SetTitleFont(63);
  ratioFrame->GetYaxis()->SetTitleSize(28);
  ratioFrame->GetXaxis()->SetTitleOffset(0.9);
  ratioFrame->GetYaxis()->SetTitleOffset(0.80);

  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    ratioGraph[iEnergy]->Draw("PZ");
  }

  TLine *unityLine = new TLine(0.0,1.,365.,1.0);
  unityLine->SetLineWidth(3);
  unityLine->SetLineColor(kBlack);
  unityLine->SetLineStyle(7);
  unityLine->Draw("SAME");

  TLegend *ratioLeg = new TLegend(.7,.13,.91,.33);
  ratioLeg->SetFillStyle(0);
  ratioLeg->SetBorderSize(0);
  ratioLeg->SetTextFont(63);
  ratioLeg->SetTextSize(22);
  ratioLeg->SetNColumns(2);
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    if (iEnergy == 2) continue;
    ratioLeg->AddEntry(ratioGraph[iEnergy],Form("%.03g",energies[iEnergy]),"P");
    ratioLeg->Draw("SAME");
  }

  ratioCanvas->Modified();
  ratioCanvas->Update();

  
}
