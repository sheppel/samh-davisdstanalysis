#include <iostream>
#include <vector>
#include <utility>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TEventList.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "usefulDataStructs.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

void ColliderQA(TString inputDataFile, TString outputFile){

  //Read the DavisDst
  DavisDstReader *davisDst = new DavisDstReader(inputDataFile);

  //Create an instance of the Particle Info Class which will use the empirical fits
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true);
  
  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);
  
  Int_t nCentBins = GetNCentralityBins();
  TString eventName = GetEventConfiguration();
  Double_t maxVz = GetZVertexCuts().second;
  Double_t minVz = GetZVertexCuts().first;
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();
  const int nParticles = (int)particles.size();

  //Create the Output File
  TFile *outFile = new TFile(outputFile,"RECREATE");
  outFile->mkdir(Form("%s",eventName.Data()));
  outFile->cd(Form("%s",eventName.Data()));
  for (int i=0; i<nParticles; i++){
    gDirectory->mkdir(Form("%s_%s",eventName.Data(),particleInfo->GetParticleName(particles.at(i),-1).Data()));
    gDirectory->mkdir(Form("%s_%s",eventName.Data(),particleInfo->GetParticleName(particles.at(i),1).Data()));
  }
  
  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;
  


  //****Data Quality Histograms****
  //Histogram for the Number of Events in each centrality Bin
  TH1D *nEventsHisto;
  
  //Event Level Plots
  TH1D *zVertexPreCuts,     *zVertexPostAllCuts;
  TH2D *xyVertexPreCuts,    *xyVertexPostAllCuts;
  TH2D *rVertexPreCuts,     *rVertexPostAllCuts;
  TH2D *refMultPreCuts,     *refMultPostAllCuts;
  TH2D *meanEtaPreCuts,     *meanEtaPostAllCuts;
  TH2D *nTofMatchesPreCuts, *nTofMatchesPostAllCuts;

  //Track Level Plots
  TH1D *etaPreCuts,               *etaPostCuts;
  TH1D *phiPreCuts,               *phiPostCuts;
  TH2D *pTPreCuts,                *pTPostCuts;
  TH2D *etaphiPreCuts,            *etaphiPostCuts;
  TH2D *dEdxPostCutsAllParticles, *invBetaPostCutsAllParticles;
  TH3D *nHitPointsPreCuts,        *nHitPointsPostCuts;
  TH3D *nFitPointsPreCuts,        *nFitPointsPostCuts;
  TH3D *nPossPointsPreCuts,       *nPossPointsPostCuts;
  TH3D *nHitsFracPreCuts,         *nHitsFracPostCuts;
  TH3D *ndEdxPointsPreCuts,       *ndEdxPointsPostCuts;
  TH3D *globalDCAPreCuts,         *globalDCAPostCuts;
  TH3D *globalpTotalPreCuts,      *globalpTotalPostCuts;
  TH3D *globalpTPreCuts,          *globalpTPostCuts;
  TH2D *tofYLocalPreCuts,         *tofYLocalPostCuts;
  TH2D *tofZLocalPreCuts,         *tofZLocalPostCuts;
  TH3D *dEdxPostCuts,             *invBetaPostCuts;

  //Track Level Plots with PID Cut (|nSigma| < 2) | [species][charge]
  TH1D *etaPreCutsID[nParticles][2], *etaPostCutsID[nParticles][2];
  TH1D *yPreCutsID[nParticles][2],   *yPostCutsID[nParticles][2];
  TH1D *phiPreCutsID[nParticles][2], *phiPostCutsID[nParticles][2];
  
  TH2D *etaphiPreCutsID[nParticles][2],       *etaphiPostCutsID[nParticles][2];
  TH2D *ypTPreCutsID[nParticles][2],          *ypTPostCutsID[nParticles][2];
  TH2D *yphiPreCutsID[nParticles][2],         *yphiPostCutsID[nParticles][2];
  TH2D *ymTm0PreCutsID[nParticles][2],        *ymTm0PostCutsID[nParticles][2];
  TH3D *ynHitPointsPreCutsID[nParticles][2],  *ynHitPointsPostCutsID[nParticles][2];
  TH3D *ynFitPointsPreCutsID[nParticles][2],  *ynFitPointsPostCutsID[nParticles][2];
  TH3D *ynPossPointsPreCutsID[nParticles][2], *ynPossPointsPostCutsID[nParticles][2];
  TH3D *ynHitsFracPreCutsID[nParticles][2],   *ynHitsFracPostCutsID[nParticles][2];
  TH3D *yglobalDCAPreCutsID[nParticles][2],   *yglobalDCAPostCutsID[nParticles][2];

  TH3D *ypTPhiPreCutsID[nParticles][2],       *ypTPhiPostCutsID[nParticles][2];
  TH3D *ypTnHitsdEdxPreCutsID[nParticles][2], *ypTnHitsdEdxPostCutsID[nParticles][2];
  TH3D *etapTPhiPreCutsID[nParticles][2],     *etapTPhiPostCutsID[nParticles][2];
  TH3D *etapTdEdxPreCutsID[nParticles][2],    *etapTdEdxPostCutsID[nParticles][2];

  
  //****Create the Histograms****
  nEventsHisto = new TH1D(Form("%s_nEventsHisto",eventName.Data()),"",nCentBins,0,nCentBins);
  
  //Definition of Event QA Histograms
  zVertexPreCuts = new TH1D(Form("%s_zVertexPreCuts",eventName.Data()),
                            Form("%s zVertex Pre Cuts;Vz (cm)",eventName.Data()),
                            450,-225,225);
  zVertexPostAllCuts = new TH1D(Form("%s_zVertexPostAllCuts",eventName.Data()),
                                Form("%s zVertex Post All Cuts;Vz (cm)",eventName.Data()),
                                maxVz-minVz+20,minVz-10,maxVz+10);
  
  xyVertexPreCuts = new TH2D(Form("%s_xyVertexPreCuts",eventName.Data()),
                             Form("%s xyVertex Pre Cuts;Vx (cm); Vy (cm)",eventName.Data()),
                             200,-10,10,200,-10,10);
  xyVertexPostAllCuts = new TH2D(Form("%s_xyVertexPostAllCuts",eventName.Data()),
                                 Form("%s xyVertex Post All Cuts;Vx (cm);Vy (cm)",eventName.Data()),
                                 200,-10,10,200,-10,10);
  
  rVertexPreCuts = new TH2D(Form("%s_rVertexPreCuts",eventName.Data()),
                            Form("%s rVertex Pre Cuts;Vr (cm);Vz (cm)",eventName.Data()),
                            20,0,10,maxVz-minVz+20,minVz-10,maxVz+10);
  rVertexPostAllCuts = new TH2D(Form("%s_rVertexPostAllCuts",eventName.Data()),
                                Form("%s rVertex Post All Cuts;Vr (cm);Vz (cm)",eventName.Data()),
                                20,0,10,maxVz-minVz+20,minVz-10,maxVz+10);
  
  refMultPreCuts = new TH2D(Form("%s_refMultPreCuts",eventName.Data()),
                            Form("%s RefMult PreCuts;RefMult;Vz (cm)",eventName.Data()),
                            1000,0,2000,maxVz-minVz+20,minVz-10,maxVz+10);
  refMultPostAllCuts = new TH2D(Form("%s_refMultPostAllCuts",eventName.Data()),
                                Form("%s RefMult Post All Cuts;RefMult;Vz (cm)",eventName.Data()),
                                1000,0,2000,maxVz-minVz+20,minVz-10,maxVz+10);
  
  meanEtaPreCuts = new TH2D(Form("%s_meanEtaPreCuts",eventName.Data()),
                            Form("%s <#eta> PreCuts;<#eta>;Vz (cm)",eventName.Data()),
                            51,-2,2,maxVz-minVz+20,minVz-10,maxVz+10);
  meanEtaPostAllCuts = new TH2D(Form("%s_meanEtaPostAllCuts",eventName.Data()),
				Form("%s <#eta> Post All Cuts;<#eta>;Vz (cm)",eventName.Data()),
				51,-2,2,maxVz-minVz+20,minVz-10,maxVz+10);
  
  nTofMatchesPreCuts = new TH2D(Form("%s_nTofMatchesPreCuts",eventName.Data()),
                                Form("%s nTofMatches PreCuts;nTofMatches;Vz (cm)",eventName.Data()),
                                250,0,500,maxVz-minVz+20,minVz-10,maxVz+10);
  nTofMatchesPostAllCuts = new TH2D(Form("%s_nTofMatchesPostAllCuts",eventName.Data()),
				    Form("%s nTofMatches Post All Cuts;nTofMatches;Vz (cm)",eventName.Data()),
				    250,0,500,maxVz-minVz+20,minVz-10,maxVz+10);

  //Definition of Track QA Histograms
  dEdxPostCutsAllParticles = new TH2D(Form("%s_EnergyLoss",eventName.Data()),
				      Form("Energy Loss in the TPC;p (GeV/c); dE/dx (KeV/cm)"),
				      1000,-5,5,1000,0,100);
  invBetaPostCutsAllParticles = new TH2D(Form("%s_InverseBeta",eventName.Data()),
					 Form("1/#beta in TOF;p (GeV/c); 1/#beta"),
					 1000,-5,5,1000,0,5);
  
  etaPreCuts = new TH1D(Form("%s_etaPreCuts",eventName.Data()),
                        Form("%s Eta Before Quality Cuts;#eta",eventName.Data()),
                        200,-2,2);
  etaPostCuts = new TH1D(Form("%s_etaPostCuts",eventName.Data()),
                         Form("%s Eta After Quality Cuts;#eta",eventName.Data()),
                         200,-2,2);
  
  phiPreCuts = new TH1D(Form("%s_phiPreCuts",eventName.Data()),
                        Form("%s Phi Before Quality Cuts;#phi",eventName.Data()),
                        200,-4,4);
  phiPostCuts = new TH1D(Form("%s_phiPostCuts",eventName.Data()),
                         Form("%s Phi After Quality Cuts;#phi",eventName.Data()),
                         200,-4,4);
  
  etaphiPreCuts = new TH2D(Form("%s_etaphiPreCuts",eventName.Data()),
                           Form("%s Phi Before Cuts;#eta;#phi",eventName.Data()),
                           200,-2,2,200,-4,4);
  etaphiPostCuts = new TH2D(Form("%s_etaphiPostCuts",eventName.Data()),
                            Form("%s Phi After Cuts;#eta;#phi",eventName.Data()),
                            200,-2,2,200,-4,4);

  nHitPointsPreCuts = new TH3D(Form("%s_nHitsPreCuts",eventName.Data()),
			       Form("%s nHits Before Cuts;#eta;p_{T} (GeV);nHits",eventName.Data()),
			       200,-2,2,5,0,2.0,45,0,45);
  nHitPointsPostCuts = new TH3D(Form("%s_nHitsPostCuts",eventName.Data()),
				Form("%s nHits After Cuts;#eta;p_{T} (GeV);nHits",eventName.Data()),
				200,-2,2,5,0,2.0,45,0,45);
  
  nFitPointsPreCuts = new TH3D(Form("%s_nFitPointsPreCuts",eventName.Data()),
			       Form("%s nFitPoints Before Cuts;#eta;p_{T} (GeV);nFitPoints",eventName.Data()),
			       200,-2,2,5,0,2.0,45,0,45);
  nFitPointsPostCuts = new TH3D(Form("%s_nFitPointsPostCuts",eventName.Data()),
				Form("%s nFitPoints After Cuts;#eta;p_{T} (GeV);nFitPoints",eventName.Data()),
				200,-2,2,5,0,2.0,45,0,45);
  
  nPossPointsPreCuts = new TH3D(Form("%s_nPossPointsPreCuts",eventName.Data()),
				Form("%s nPossPoints Before Cuts;#eta;p_{T} (GeV);nPossPoints",eventName.Data()),
				200,-2,2,5,0,2.0,45,0,45);
  nPossPointsPostCuts = new TH3D(Form("%s_nPossPointsPostCuts",eventName.Data()),
				 Form("%s nPossPoints After Cuts;#eta;p_{T} (GeV);nPossPoints",eventName.Data()),
				 200,-2,2,5,0,2.0,45,0,45);
  
  nHitsFracPreCuts = new TH3D(Form("%s_nHitsFracPreCuts",eventName.Data()),
			      Form("%s nHitsFrac Before Cuts;#eta;p_{T} (GeV);nHitsFrac",eventName.Data()),
			      200,-2,2,5,0,2.0,100,0,1);
  nHitsFracPostCuts = new TH3D(Form("%s_nHitsFracPostCuts",eventName.Data()),
			       Form("%s nHitsFrac After Cuts;#eta;p_{T} (GeV);nHitsFrac",eventName.Data()),
			       200,-2,2,5,0,2.0,100,0,1);
  
  ndEdxPointsPreCuts = new TH3D(Form("%s_ndEdxPointsPreCuts",eventName.Data()),
				Form("%s ndEdxPoints Before Cuts;#eta;p_{T} (GeV);ndEdxPoints",eventName.Data()),
				200,-2,2,5,0,2.0,45,0,45);
  ndEdxPointsPostCuts = new TH3D(Form("%s_ndEdxPointsPostCuts",eventName.Data()),
				 Form("%s ndEdxPoints After Cuts;#eta;p_{T} (GeV);ndEdxPoints",eventName.Data()),
				 200,-2,2,5,0,2.0,45,0,45);
  
  globalDCAPreCuts = new TH3D(Form("%s_globalDCAPreCuts",eventName.Data()),
			      Form("%s Global DCA Before Cuts;#eta;p_{T} (GeV);globalDCA",eventName.Data()),
			      200,-2,2,5,0,2.0,50,0,10);
  globalDCAPostCuts = new TH3D(Form("%s_globalDCAPostCuts",eventName.Data()),
			       Form("%s Global DCA After Cuts;#eta;p_{T} (GeV);globalDCA",eventName.Data()),
			       200,-2,2,5,0,2.0,50,0,10);

  globalpTotalPreCuts = new TH3D(Form("%s_globalpTotalPreCuts",eventName.Data()),
				 Form("%s Global Vs. Primary Momentum Before Cuts;#eta;|p^{Global}| (GeV);|p^{Primary}| (GeV)",eventName.Data()),
				 200,-2,2,50,0,5,50,0,5);
  globalpTotalPostCuts = new TH3D(Form("%s_globalpTotalPostCuts",eventName.Data()),
				  Form("%s Global Vs. Primary Momentum After Cuts;#eta;|p^{Global}| (GeV);|p^{Primary}| (GeV)",eventName.Data()),
				  200,-2,2,50,0,5,50,0,5);
  
  globalpTPreCuts = new TH3D(Form("%s_globalpTPreCuts",eventName.Data()),
			     Form("%s Global Vs. Primary p_{T} Before Cuts;#eta;|p_{T}^{Global}| (GeV);|p_{T}^{Primary}| (GeV)",eventName.Data()),
			     200,-2,2,50,0,5,50,0,5);
  globalpTPostCuts = new TH3D(Form("%s_globalpTPostCuts",eventName.Data()),
			      Form("%s Global Vs. Primary p_{T} After Cuts;#eta;|p_{T}^{Global}| (GeV);|p_{T}^{Primary}| (GeV)",eventName.Data()),
			      200,-2,2,50,0,5,50,0,5);
  
  tofYLocalPreCuts = new TH2D(Form("%s_tofYLocalPreCuts",eventName.Data()),
			      Form("%s ToF Y-Local Before Cuts;#eta;ToF Y-Local (cm)",eventName.Data()),
			      200,-2,2,150,-3,3);
  tofYLocalPostCuts = new TH2D(Form("%s_tofYLocalPostCuts",eventName.Data()),
			       Form("%s ToF Y-Local After Cuts;#eta;ToF Y-Local (cm)",eventName.Data()),
			       200,-2,2,150,-3,3);
  
  tofZLocalPreCuts = new TH2D(Form("%s_tofZLocalPreCuts",eventName.Data()),
			      Form("%s ToF Z-Local Before Cuts;#eta;ToF Z-Local (cm)",eventName.Data()),
			      200,-2,2,150,-3,3);
  tofZLocalPostCuts = new TH2D(Form("%s_tofZLocalPostCuts",eventName.Data()),
			       Form("%s ToF Z-Local After Cuts;#eta;ToF Z-Local (cm)",eventName.Data()),
			       200,-2,2,150,-3,3);
  
  pTPreCuts = new TH2D(Form("%s_pTPreCuts",eventName.Data()),
		       Form("%s pT Before Cuts;#eta;p_{T} (GeV)",eventName.Data()),
		       200,-2,2,200,0,5);
  pTPostCuts = new TH2D(Form("%s_pTPostCuts",eventName.Data()),
			Form("%s pT After Cuts;#eta;p_{T} (GeV)",eventName.Data()),
			200,-2,2,200,0,5);
  
  dEdxPostCuts = new TH3D(Form("%s_dEdxPostCuts",eventName.Data()),
			  Form("%s Energy Loss After Cuts;#eta;p*q (GeV);dEdx (KeV/cm)",eventName.Data()),
			  41,-2.05,2.05,600,-6,6,1000,0,10);
  
  invBetaPostCuts = new TH3D(Form("%s_invBetaPostCuts",eventName.Data()),
			     Form("%s Inv Beta After Cuts;#eta;p*q (GeV);#beta^{-1}",eventName.Data()),
			     41,-2.05,2.05,600,-6,6,200,0,2);
  

  //Definition of Track QA Histograms with PID Cuts
  for (int iParticle=0; iParticle<nParticles; iParticle++){

    int species = particles.at(iParticle);
    TString yieldName =eventName;
    
    int charges[2] = {-1,1};
    for (int iCharge=0; iCharge<2; iCharge++){
      
      etaPreCutsID[iParticle][iCharge] = new TH1D(Form("%s_etaPreCuts",yieldName.Data()),
						  Form("%s Eta Pre Cuts;#eta",yieldName.Data()),
						  200,-2,2);
      yPreCutsID[iParticle][iCharge] = new TH1D(Form("%s_yPreCuts",yieldName.Data()),
						Form("%s Rapidity Pre Cuts;y",yieldName.Data()),
						nRapidityBins,rapidityMin,rapidityMax);
      phiPreCutsID[iParticle][iCharge] = new TH1D(Form("%s_phiPreCuts",yieldName.Data()),
						  Form("%s #phi Pre Cuts;#phi",yieldName.Data()),
						  200,-4,4);
      etaphiPreCutsID[iParticle][iCharge] = new TH2D(Form("%s_etaphiPreCuts",yieldName.Data()),
						     Form("%s #eta Vs. #phi Pre Cuts;#eta;#phi",yieldName.Data()),
						     200,-2,2,200,-4,4);
      ypTPreCutsID[iParticle][iCharge] = new TH2D(Form("%s_ypTPreCuts",yieldName.Data()),
						  Form("%s Rapidity Vs. p_{T} Pre Cuts;y;p_{T} (GeV)",yieldName.Data()),
						  nRapidityBins,rapidityMin,rapidityMax,400,0,2);
      yphiPreCutsID[iParticle][iCharge] = new TH2D(Form("%s_yphiPreCuts",yieldName.Data()),
						   Form("%s Rapidity Vs. #phi Pre Cuts;y;#phi",yieldName.Data()),
						   nRapidityBins,rapidityMin,rapidityMax,400,-4,4);
      ymTm0PreCutsID[iParticle][iCharge] = new TH2D(Form("%s_ymTm0PreCuts",yieldName.Data()),
						    Form("%s Rapidity Vs. m_{T}-m_{0} Pre Cuts;y;m_{T}-m_{0}",yieldName.Data()),
						    nRapidityBins,rapidityMin,rapidityMax,400,0,2);
      ynHitPointsPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynHitPointsPreCuts",yieldName.Data()),
							  Form("%s Rapidity Vs. p_{T} Vs. nHitPoints Pre Cuts;y;p_{T} (GeV);nHitPoints",yieldName.Data()),
							  nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      ynFitPointsPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynFitPointsPreCuts",yieldName.Data()),
							  Form("%s Rapidity Vs. p_{T} Vs. nFitPoints Pre Cuts;y;p_{T} (GeV);nFitPoints",yieldName.Data()),
							  nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      ynPossPointsPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynPossPointsPreCuts",yieldName.Data()),
							   Form("%s Rapidity Vs. p_{T} Vs. nPossPoints Pre Cuts;y;p_{T} (GeV);nPossPoints",yieldName.Data()),
							   nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      ynHitsFracPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynHitsFracPreCuts",yieldName.Data()),
							 Form("%s Rapidity Vs. p_{T} Vs. nHitsFrac Pre Cuts;y;p_{T} (GeV);nHitsFrac",yieldName.Data()),
							 nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,100,0,1);
      yglobalDCAPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_yglobalDCAPreCuts",yieldName.Data()),
							 Form("%s Rapidity Vs. p_{T} Vs. GlobalDCA Pre Cuts;y;p_{T} (GeV);globalDCA (cm)",yieldName.Data()),
							 nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,100,0,10);
      ypTPhiPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_ypTPhiPreCuts",yieldName.Data()),
						     Form("%s Rapidity Vs. p_{T} Vs. #phi Pre Cuts;y;p_{T} (GeV);#phi",yieldName.Data()),
						     nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,400,-4,4);
      etapTPhiPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_etapTPhiPreCuts",yieldName.Data()),
						       Form("%s #eta Vs. p_{T} Vs. #phi Pre Cuts;y;p_{T} (GeV);#phi",yieldName.Data()),
						       nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,400,-4,4);
      ypTnHitsdEdxPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_ypTnHitsdEdxPreCuts",yieldName.Data()),
							   Form("%s Rapidity Vs. p_{T} Vs. nHitsdEdx Pre Cuts;y;p_{T} (GeV);nHitsdEdx",yieldName.Data()),
							   nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);


      etapTdEdxPreCutsID[iParticle][iCharge] = new TH3D(Form("%s_etapTdEdxPreCuts",yieldName.Data()),
							Form("%s #eta Vs. p_{T} Vs. dEdx;#eta;p_{T} (GeV);dEdx (KeV/cm)",yieldName.Data()),
							nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,100,0,10);
      
      etaPostCutsID[iParticle][iCharge] = new TH1D(Form("%s_etaPostCuts",yieldName.Data()),
						   Form("%s Eta Post Cuts;#eta",yieldName.Data()),
						   200,-2,2);
      yPostCutsID[iParticle][iCharge] = new TH1D(Form("%s_yPostCuts",yieldName.Data()),
						 Form("%s Rapidity Post Cuts;y",yieldName.Data()),
						 nRapidityBins,rapidityMin,rapidityMax);
      phiPostCutsID[iParticle][iCharge] = new TH1D(Form("%s_phiPostCuts",yieldName.Data()),
						   Form("%s #phi Post Cuts;#phi",yieldName.Data()),
						   200,-4,4);
      etaphiPostCutsID[iParticle][iCharge] = new TH2D(Form("%s_etaphiPostCuts",yieldName.Data()),
						      Form("%s #eta Vs. #phi Post Cuts;#eta;#phi",yieldName.Data()),
						      200,-2,2,200,-4,4);
      ypTPostCutsID[iParticle][iCharge] = new TH2D(Form("%s_ypTPostCuts",yieldName.Data()),
						   Form("%s Rapidity Vs. p_{T} Post Cuts;y;p_{T} (GeV)",yieldName.Data()),
						   nRapidityBins,rapidityMin,rapidityMax,400,0,2);
      yphiPostCutsID[iParticle][iCharge] = new TH2D(Form("%s_yphiPostCuts",yieldName.Data()),
						    Form("%s Rapidity Vs. #phi Post Cuts;y;#phi",yieldName.Data()),
						    nRapidityBins,rapidityMin,rapidityMax,400,-4,4);
      ymTm0PostCutsID[iParticle][iCharge] = new TH2D(Form("%s_ymTm0PostCuts",yieldName.Data()),
						     Form("%s Rapidity Vs. m_{T}-m_{0} Post Cuts;y;m_{T}-m_{0}",yieldName.Data()),
						     nRapidityBins,rapidityMin,rapidityMax,400,0,2);
      ynHitPointsPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynHitPointsPostCuts",yieldName.Data()),
							   Form("%s Rapidity Vs. p_{T} Vs. nHitPoints Post Cuts;y;p_{T} (GeV);nHitPoints",yieldName.Data()),
							   nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      ynFitPointsPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynFitPointsPostCuts",yieldName.Data()),
							   Form("%s Rapidity Vs. p_{T} Vs. nFitPoints Post Cuts;y;p_{T} (GeV);nFitPoints",yieldName.Data()),
							   nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      ynPossPointsPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynPossPointsPostCuts",yieldName.Data()),
							    Form("%s Rapidity Vs. p_{T} Vs. nPossPoints Post Cuts;y;p_{T} (GeV);nPossPoints",yieldName.Data()),
							    nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      ynHitsFracPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_ynHitsFracPostCuts",yieldName.Data()),
							  Form("%s Rapidity Vs. p_{T} Vs. nHitsFrac Post Cuts;y;p_{T} (GeV);nHitsFrac",yieldName.Data()),
							  nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,100,0,1);
      yglobalDCAPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_yglobalDCAPostCuts",yieldName.Data()),
							  Form("%s Rapidity Vs. p_{T} Vs. GlobalDCA Post Cuts;y;p_{T} (GeV);globalDCA (cm)",yieldName.Data()),
							  nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,5);
      ypTPhiPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_ypTPhiPostCuts",yieldName.Data()),
						      Form("%s Rapidity Vs. p_{T} Vs. #phi Post Cuts;y;p_{T} (GeV);#phi",yieldName.Data()),
						      nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,400,-4,4);
      etapTPhiPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_etapTPhiPostCuts",yieldName.Data()),
							Form("%s #eta Vs. p_{T} Vs. #phi Post Cuts;y;p_{T} (GeV);#phi",yieldName.Data()),
							nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,400,-4,4);
      ypTnHitsdEdxPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_ypTnHitsdEdxPostCuts",yieldName.Data()),
							    Form("%s Rapidity Vs. p_{T} Vs. nHitsdEdx Post Cuts;y;p_{T} (GeV);nHitsdEdx",yieldName.Data()),
							    nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,50,0,50);
      
      etapTdEdxPostCutsID[iParticle][iCharge] = new TH3D(Form("%s_etapTdEdxPostCuts",yieldName.Data()),
							 Form("%s #eta Vs. p_{T} Vs. dEdx Post Cuts;#eta;p_{T} (GeV);dEdx (KeV/cm)",yieldName.Data()),
							 nRapidityBins,rapidityMin,rapidityMax,5,0,2.0,100,0,10);
      
    }//End Loop Over Charge
    
  }//End Loop Over Particles
  
  //****Loop Over the Events****
  const long nEvents = davisDst->GetEntries();
  for (long iEntry=0; iEntry<nEvents; iEntry++){

    event = davisDst->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;
    
    //Loop Over the Vertices
    for (Int_t iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){

      //Get the Vertex
      primaryVertex = event->GetPrimaryVertex(iVertex);
      
      //Fill The Pre Cut Histograms
      zVertexPreCuts->Fill(primaryVertex->GetZVertex());
      xyVertexPreCuts->Fill(primaryVertex->GetXVertex(),primaryVertex->GetYVertex());
      rVertexPreCuts->Fill(sqrt(pow(primaryVertex->GetXVertex(),2)+pow(primaryVertex->GetYVertex(),2)),
			       primaryVertex->GetZVertex());
      refMultPreCuts->Fill(primaryVertex->GetRefMult(),primaryVertex->GetZVertex());
      meanEtaPreCuts->Fill(event->GetMeanEta(),primaryVertex->GetZVertex());
      nTofMatchesPreCuts->Fill(primaryVertex->GetNTofMatches(),primaryVertex->GetZVertex());

      //Apply the Cuts
      if (!IsGoodVertex(primaryVertex))
        continue;

      //Get the Centrality Bin
      UInt_t triggerID = event->GetTriggerID(&allowedTriggers);
      Int_t centralityVariable = GetCentralityVariable(primaryVertex);
      Int_t centralityBin      = GetCentralityBin(centralityVariable,triggerID,
                                                  primaryVertex->GetZVertex());

      //Fill The Post Cut Histograms
      nEventsHisto->Fill(centralityBin);
      zVertexPostAllCuts->Fill(primaryVertex->GetZVertex());
      xyVertexPostAllCuts->Fill(primaryVertex->GetXVertex(),primaryVertex->GetYVertex());
      rVertexPostAllCuts->Fill(sqrt(pow(primaryVertex->GetXVertex(),2)+pow(primaryVertex->GetYVertex(),2)),
			       primaryVertex->GetZVertex());
      refMultPostAllCuts->Fill(primaryVertex->GetRefMult(),primaryVertex->GetZVertex());
      meanEtaPostAllCuts->Fill(event->GetMeanEta(),primaryVertex->GetZVertex());
      nTofMatchesPostAllCuts->Fill(primaryVertex->GetNTofMatches(),primaryVertex->GetZVertex());

      //Loop Over the Tracks
      for (Int_t iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){

	//Get the Track
	track = primaryVertex->GetPrimaryTrack(iTrack);
	
	//Fill the Pre Track Cut Histos
	etaPreCuts->Fill(track->GetEta());
	phiPreCuts->Fill(track->GetPhi());
	pTPreCuts->Fill(track->GetEta(),track->GetPt());
	etaphiPreCuts->Fill(track->GetEta(),track->GetPhi());

	//Loop Over the Particle Spcies
	for (int iParticle=0; iParticle<nParticles; iParticle++){

	  int species = particles.at(iParticle);
	  
	  //Apply the PID Cut
	  if (fabs(particleInfo->ComputeZTPC(track->GetdEdx()*pow(10,6),track->GetPt(),track->GetPz(),species)
		   /particleInfo->GetTPCWidth()) > 2.0)
	    continue;
	  
	  int chargeIndex = 0;
	  if (track->GetCharge() < 0)
	    chargeIndex = 0;
	  else
	    chargeIndex = 1;

	  Double_t mass     = particleInfo->GetParticleMass(species);
	  Double_t rapidity = track->GetRapidity(mass);
	  Double_t mTm0     = track->GetmTm0(mass);
	  
	  etaPreCutsID[iParticle][chargeIndex]->Fill(track->GetEta());
	  yPreCutsID[iParticle][chargeIndex]->Fill(rapidity);
	  phiPreCutsID[iParticle][chargeIndex]->Fill(track->GetPhi());
	  
	  etaphiPreCutsID[iParticle][chargeIndex]->Fill(track->GetEta(),track->GetPhi());
	  ypTPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt());
	  yphiPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPhi());
	  ymTm0PreCutsID[iParticle][chargeIndex]->Fill(rapidity,mTm0);
	  
	  ynHitPointsPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetHits());
	  ynFitPointsPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetHitsFit());
	  ynPossPointsPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetHitsPoss());
	  ynHitsFracPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetNFrac());
	  yglobalDCAPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetGlobalDCA()->Mag());
	  ypTPhiPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetPhi());
	  ypTnHitsdEdxPreCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetdEdxHits());
	  etapTPhiPreCutsID[iParticle][chargeIndex]->Fill(track->GetEta(),track->GetPt(),track->GetPhi());
	  etapTdEdxPreCutsID[iParticle][chargeIndex]->Fill(track->GetEta(),track->GetPt(),track->GetdEdxHits());
	  
	}//End Loop Over Particles
	
	//Apply the Track Cuts
        if (!IsGoodTrack(track))
          continue;

	//Fill the Post Track Cut Histos
	etaPostCuts->Fill(track->GetEta());
	phiPostCuts->Fill(track->GetPhi());
	pTPostCuts->Fill(track->GetEta(),track->GetPt());
	etaphiPostCuts->Fill(track->GetEta(),track->GetPhi());
	dEdxPostCutsAllParticles->Fill(track->GetPTotal()*track->GetCharge(),track->GetdEdx()*pow(10,6));
	if (IsGoodTofTrack(track))
	  invBetaPostCutsAllParticles->Fill(track->GetPTotal()*track->GetCharge(),1.0/track->GetTofBeta());

	//Loop Over the Particle Spcies
	for (int iParticle=0; iParticle<nParticles; iParticle++){

	  int species = particles.at(iParticle);
	  
	  //Apply the PID Cut
	  if (fabs(particleInfo->GetPercentErrorTPC(track->GetdEdx()*pow(10,6), track->GetPTotal(),species))
	      > 2.0 * .08)
	    continue;
	  
	  int chargeIndex = 0;
	  if (track->GetCharge() < 0)
	    chargeIndex = 0;
	  else
	    chargeIndex = 1;

	  Double_t mass     = particleInfo->GetParticleMass(species);
	  Double_t rapidity = track->GetRapidity(mass);
	  Double_t mTm0     = track->GetmTm0(mass);
	  
	  etaPostCutsID[iParticle][chargeIndex]->Fill(track->GetEta());
	  yPostCutsID[iParticle][chargeIndex]->Fill(rapidity);
	  phiPostCutsID[iParticle][chargeIndex]->Fill(track->GetPhi());
	  
	  etaphiPostCutsID[iParticle][chargeIndex]->Fill(track->GetEta(),track->GetPhi());
	  ypTPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt());
	  yphiPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPhi());
	  ymTm0PostCutsID[iParticle][chargeIndex]->Fill(rapidity,mTm0);
	  
	  ynHitPointsPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetHits());
	  ynFitPointsPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetHitsFit());
	  ynPossPointsPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetHitsPoss());
	  ynHitsFracPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetNFrac());
	  yglobalDCAPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetGlobalDCA()->Mag());
	  ypTPhiPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetPhi());
	  ypTnHitsdEdxPostCutsID[iParticle][chargeIndex]->Fill(rapidity,track->GetPt(),track->GetdEdxHits());
	  etapTPhiPostCutsID[iParticle][chargeIndex]->Fill(track->GetEta(),track->GetPt(),track->GetPhi());
	  etapTdEdxPostCutsID[iParticle][chargeIndex]->Fill(track->GetEta(),track->GetPt(),track->GetdEdxHits());
	  
	}//End Loop Over Particles
	
      }//End Loop Over Tracks
      
    }//End Loop Over Vertices
      

  }//End Loop Over Events
  

  //Save
  outFile->cd();
  outFile->cd(Form("%s",eventName.Data()));
  
  nEventsHisto->Write();
  zVertexPreCuts->Write();
  zVertexPostAllCuts->Write();
  xyVertexPreCuts->Write();
  xyVertexPostAllCuts->Write();
  rVertexPreCuts->Write();
  rVertexPostAllCuts->Write();
  refMultPreCuts->Write();
  refMultPostAllCuts->Write();
  meanEtaPreCuts->Write();
  meanEtaPostAllCuts->Write();
  nTofMatchesPreCuts->Write();
  nTofMatchesPostAllCuts->Write();

  dEdxPostCutsAllParticles->Write();
  invBetaPostCutsAllParticles->Write();
  etaPreCuts->Write();
  etaPostCuts->Write();
  phiPreCuts->Write();
  phiPostCuts->Write();
  etaphiPreCuts->Write();
  etaphiPostCuts->Write();

  for (int iParticle=0; iParticle<nParticles; iParticle++){

    int charges[2] = {-1,1};
    for (int iCharge=0; iCharge<2; iCharge++){
      outFile->cd(Form("%s/%s_%s",eventName.Data(),eventName.Data(),
			  particleInfo->GetParticleName(particles.at(iParticle),charges[iCharge]).Data()));

      etaPreCutsID[iParticle][iCharge]->Write();
      etaPostCutsID[iParticle][iCharge]->Write();
      yPreCutsID[iParticle][iCharge]->Write();
      yPostCutsID[iParticle][iCharge]->Write();
      phiPreCutsID[iParticle][iCharge]->Write();
      phiPostCutsID[iParticle][iCharge]->Write();
      etaphiPreCutsID[iParticle][iCharge]->Write();
      etaphiPostCutsID[iParticle][iCharge]->Write();
      ypTPreCutsID[iParticle][iCharge]->Write();
      ypTPostCutsID[iParticle][iCharge]->Write();
      yphiPreCutsID[iParticle][iCharge]->Write();
      yphiPostCutsID[iParticle][iCharge]->Write();
      ymTm0PreCutsID[iParticle][iCharge]->Write();
      ymTm0PostCutsID[iParticle][iCharge]->Write();
      ynHitPointsPreCutsID[iParticle][iCharge]->Write();
      ynHitPointsPostCutsID[iParticle][iCharge]->Write();
      ynFitPointsPreCutsID[iParticle][iCharge]->Write();
      ynFitPointsPostCutsID[iParticle][iCharge]->Write();
      ynPossPointsPreCutsID[iParticle][iCharge]->Write();
      ynPossPointsPostCutsID[iParticle][iCharge]->Write();
      ynHitsFracPreCutsID[iParticle][iCharge]->Write();
      ynHitsFracPostCutsID[iParticle][iCharge]->Write();
      yglobalDCAPreCutsID[iParticle][iCharge]->Write();
      yglobalDCAPostCutsID[iParticle][iCharge]->Write();      
      ypTPhiPreCutsID[iParticle][iCharge]->Write();
      ypTPhiPostCutsID[iParticle][iCharge]->Write();
      ypTnHitsdEdxPreCutsID[iParticle][iCharge]->Write();
      ypTnHitsdEdxPostCutsID[iParticle][iCharge]->Write();
      etapTPhiPreCutsID[iParticle][iCharge]->Write();
      etapTPhiPostCutsID[iParticle][iCharge]->Write();
      etapTdEdxPreCutsID[iParticle][iCharge]->Write();
      etapTdEdxPostCutsID[iParticle][iCharge]->Write();
      
    }//End Loop Over Charges

  }//End Loop Over Particles
  
}
