//Identify bad runs by comparing the eta phi distributions between
//data and embedding

#include <iostream>
#include <utility>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>

std::pair<int,double> GetBestSector(Double_t zSign, TH1D *sectorHisto);

//MAIN____________________________________________
void IdentifyBadRunsEtaPhi(TString dataFileName, TString embFileName,
			   TString outFileName){

  //Count run number for which there is no embedding
  int totalRuns(0);
  int noEmbHisto(0);
  
  //Open the data and embeddign file
  TFile *dataFile = new TFile(dataFileName,"READ");
  TFile *embFile  = new TFile(embFileName,"READ");

  TH1D *dataHisto = NULL;
  TH1D *embHisto  = NULL;
  TH1D *matchHisto = NULL;

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,1500,500);
  canvas->Divide(3,1);

  const int nSectors(24);
  std::vector<TGraph *> sectorPerformance(nSectors);
  std::vector<TGraph *> modelRealityPerformance(nSectors);
  for (int iSector=0; iSector<nSectors; iSector++){
    sectorPerformance.at(iSector) = new TGraph();
    sectorPerformance.at(iSector)->SetName(Form("TPCSector_%d",iSector+1));
    sectorPerformance.at(iSector)->SetMarkerStyle(kDot);
    sectorPerformance.at(iSector)->SetMarkerColor(40+iSector);
    sectorPerformance.at(iSector)->SetLineColor(sectorPerformance.at(iSector)->GetMarkerColor());

    modelRealityPerformance.at(iSector) = new TGraph();
    modelRealityPerformance.at(iSector)->SetName(Form("TPCSector_%d",iSector+1));
    modelRealityPerformance.at(iSector)->SetMarkerStyle(kDot);
    modelRealityPerformance.at(iSector)->SetMarkerColor(40+iSector);
    modelRealityPerformance.at(iSector)->SetLineColor(modelRealityPerformance.at(iSector)->GetMarkerColor());
    
  }
  
  //Compare each data eta phi histo to the one in the embedding
  //file run by run. Compute the chi2 and determine if they are similar
  dataFile->cd("EtaPhiHistos");
  TList *keys = (TList *)gDirectory->GetListOfKeys();
  Int_t nHistos = keys->GetEntries();

  //Get All the Run Numbers
  std::vector<int> runNumbers;
  for (int iHisto=0; iHisto<nHistos; iHisto++){

    TString name(keys->At(iHisto)->GetName());
    
    if (!name.Contains("Sector"))
      continue;
    
    TObjArray *arr = name.Tokenize("_");
    TString str1(((TObjString *)arr->At(1))->GetName());
    runNumbers.push_back(str1.Atoi());
    
  }//End Get All Run Numbers
  
  //Sort Run Numbers from Least to Greatest (Time Ordered)
  int indexArr[runNumbers.size()];
  TMath::Sort((int)runNumbers.size(),&runNumbers.at(0),indexArr,false);
  std::vector<int> sortedRunNumbers;
  for (unsigned int i=0; i<runNumbers.size(); i++){
    sortedRunNumbers.push_back(runNumbers.at(indexArr[i]));
  }
  
  canvas->cd(2);
  TH1F *frame = gPad->DrawFrame(0,0,sortedRunNumbers.size()+10,1.2);
  frame->SetTitle("Relative TPC Sector Performance;RunIndex (Chronological);Relative Efficiency");

  canvas->cd(3);
  TH1F *frame2 = gPad->DrawFrame(0,0,sortedRunNumbers.size()+10,2.0);
  frame2->SetTitle("Model Vs. Data TPC Sector Performance;RunIndex (Chronological);STARSIM(Geant)/Data");

  std::vector<double> globalModelRealityPerformance;

  //Loop Over the Histograms
  for (unsigned int iHisto=0; iHisto<sortedRunNumbers.size(); iHisto++){
    
    dataHisto = NULL;
    embHisto = NULL;
    
    dataHisto = (TH1D *)dataFile->Get(Form("EtaPhiHistos/TPCSectorID_%d",sortedRunNumbers.at(iHisto)));
    
    matchHisto = (TH1D *)embFile->Get(Form("tpcMatchSectorID_%d",sortedRunNumbers.at(iHisto)));
    embHisto   = (TH1D *)embFile->Get(Form("tpcEmbSectorID_%d",sortedRunNumbers.at(iHisto)));
   
    if (!dataHisto)
      continue;

    totalRuns++;
    
    if (!embHisto || !matchHisto){
      noEmbHisto++;
      continue;
    }

    dataHisto->Sumw2();
    embHisto->Sumw2();
    matchHisto->Sumw2();
    
  
    dataHisto->SetLineColor(kRed);
    embHisto->SetLineColor(kBlack);

    dataHisto->SetLineWidth(2);
    embHisto->SetLineWidth(2);

    //Compute the model (true) efficiency for the embedding 
    embHisto->Divide(matchHisto,embHisto);
    
    //Get the Best sector on each side
    std::pair<int,double> posZSide = GetBestSector(1,dataHisto);
    std::pair<int,double> negZSide = GetBestSector(-1,dataHisto);
    
    //Scale the Data and Embedding Histograms
    for (int iSector=1; iSector<=24; iSector++){

      dataHisto->SetBinContent(iSector,dataHisto->GetBinContent(iSector)/
			       (iSector<=12 ? posZSide.second : negZSide.second));

    }

    //Scale up emb histo to match the data to remove difference between
    //relative efficiency and true efficiency
    Double_t ratio = (dataHisto->GetBinContent(dataHisto->GetMaximumBin())) /
      (embHisto->GetBinContent(embHisto->GetMaximumBin()));
    for (int iSector=1; iSector<=24; iSector++){
      embHisto->SetBinContent(iSector,ratio * embHisto->GetBinContent(iSector));
    }
  
    for (int iSector=0; iSector<nSectors; iSector++){      
      
      sectorPerformance.at(iSector)->SetPoint(sectorPerformance.at(iSector)->GetN(),
					      iHisto,
					      dataHisto->GetBinContent(dataHisto->FindBin(iSector+1)));

      modelRealityPerformance.at(iSector)->SetPoint(modelRealityPerformance.at(iSector)->GetN(),
						    iHisto,
						    embHisto->GetBinContent(embHisto->FindBin(iSector+1)) /
						    dataHisto->GetBinContent(dataHisto->FindBin(iSector+1)));
    }
    
    
    
    canvas->cd(1);
    TH1F *frame1 = gPad->DrawFrame(.5,0,24.5,1.1);
    frame1->SetTitle(Form("TPC Sector Performance | RunID=%d;TPC Sector ID; Relative Efficiency",sortedRunNumbers.at(iHisto)));
    dataHisto->Draw("HIST SAME");
    embHisto->Draw("HIST SAME");
    gPad->Update();
    
    canvas->Modified();
    canvas->Update();
    //gSystem->Sleep(10);


    
  }//End Loop Over Histos

  canvas->cd(2);
  for (int iSector=nSectors-1; iSector>=0; iSector--){
    sectorPerformance.at(iSector)->Draw("L");
  }

  canvas->cd(3);
  for (int iSector=nSectors-1; iSector>=0; iSector--){
    modelRealityPerformance.at(iSector)->Draw("L");
  }

  
  for (int i=0; i<modelRealityPerformance.at(0)->GetN(); i++){

    std::vector<double> vals(nSectors);
    std::vector<double> magVals(nSectors);
    for (int iSector=0; iSector<nSectors; iSector++){
      vals.at(iSector) = modelRealityPerformance.at(iSector)->GetY()[i];
      magVals.at(iSector) = fabs(1.0 - modelRealityPerformance.at(iSector)->GetY()[i]);
    }

    //Sort the Magnitude Values, throw away the largest four
    std::vector<double> usefulVals(nSectors-5);
    std::vector<double> sortedMagVals(nSectors);
    int sortIndex[sortedMagVals.size()];
    TMath::Sort((int)sortedMagVals.size(),&magVals.at(0),sortIndex,false);    
    for (unsigned int j=0; j<usefulVals.size(); j++){
      usefulVals.at(j) = vals.at(sortIndex[j]);
      globalModelRealityPerformance.push_back(vals.at(sortIndex[j]));
    }
     
  }
  
  double globalAvg = TMath::Mean((int)globalModelRealityPerformance.size(),&globalModelRealityPerformance.at(0));
  double globalRMS = 4.0*TMath::RMS((int)globalModelRealityPerformance.size(),&globalModelRealityPerformance.at(0));

  TF1 *globalAvgLine = new TF1("globalAvgLine","pol0",0,sortedRunNumbers.size());
  globalAvgLine->SetParameter(0,globalAvg);
  globalAvgLine->SetLineColor(kRed);
  globalAvgLine->SetLineWidth(4);
  globalAvgLine->Draw("SAME");

  TF1 *globalRMSLowLine = new TF1("globalRMSLowLine","pol0",0,sortedRunNumbers.size());
  globalRMSLowLine->SetParameter(0,globalAvg-globalRMS);
  globalRMSLowLine->SetLineColor(kRed);
  globalRMSLowLine->SetLineStyle(7);
  globalRMSLowLine->SetLineWidth(4);
  globalRMSLowLine->Draw("SAME");

  TF1 *globalRMSHighLine = new TF1("globalRMSHighLine","pol0",0,sortedRunNumbers.size());
  globalRMSHighLine->SetParameter(0,globalAvg+globalRMS);
  globalRMSHighLine->SetLineColor(kRed);
  globalRMSHighLine->SetLineStyle(7);
  globalRMSHighLine->SetLineWidth(4);
  globalRMSHighLine->Draw("SAME");
  
  canvas->Update();
  
  cout <<totalRuns <<" " <<noEmbHisto <<endl;

  //Construct run by run bad sector map
  std::vector<std::pair<int,std::vector<bool> > > badSectorMap(sortedRunNumbers.size(),std::pair<int,std::vector<bool> >
							       (0,std::vector<bool>(nSectors,false)));

  //First do all the run numbers that we have emedding data for
  for (int iPoint=0; iPoint<modelRealityPerformance.at(0)->GetN(); iPoint++){

    //Set the Run Number
    int runIndex = modelRealityPerformance.at(0)->GetX()[iPoint];
    badSectorMap.at(runIndex).first = sortedRunNumbers.at(runIndex);
    
    //Loop Over each sector, if its model/data ratio is outside of the
    //limits then change its bad status to true
    for (int iSector=0; iSector<nSectors; iSector++){

      Double_t modelRealityValue = modelRealityPerformance.at(iSector)->GetY()[iPoint];
      if (modelRealityValue > globalRMSHighLine->Eval(runIndex) ||
	  modelRealityValue < globalRMSLowLine->Eval(runIndex))
	badSectorMap.at(runIndex).second.at(iSector) = true;
      else
	badSectorMap.at(runIndex).second.at(iSector) = false;
      
    }//End Loop Over Sectors
    
  }//End Loop Over Embedding Runs

  //Next do all the run numbers that do not have embedding data
  for (unsigned int iRunIndex=0; iRunIndex<badSectorMap.size(); iRunIndex++){

    //If this entry already has a nonzero run number assigned then skip it
    //since it was a run that we had embedding for
    if (badSectorMap.at(iRunIndex).first != 0)
      continue;

    //Set the Run Number
    badSectorMap.at(iRunIndex).first = sortedRunNumbers.at(iRunIndex);
    
    //Handle End Point Exceptions
    if (iRunIndex == 0){
      int tempIter=iRunIndex+1;
      while (badSectorMap.at(tempIter).first == 0){
	tempIter++;
      }//End Look Ahead
      for (unsigned int iSector=0; iSector<nSectors; iSector++){
	badSectorMap.at(iRunIndex).second.at(iSector) = badSectorMap.at(tempIter).second.at(iSector);
      }//End Loop Over Sectors
      continue;
    }

    //Loop Over all the sectors and impute the goodness or badness of each
    //by assuming that the sector's status has not changed from the previous run
    for (int iSector=0; iSector<nSectors; iSector++){
      badSectorMap.at(iRunIndex).second.at(iSector) = badSectorMap.at(iRunIndex-1).second.at(iSector);      
    }
    
  }

  for (unsigned int iRunIndex=0; iRunIndex<badSectorMap.size(); iRunIndex++){

    cout <<badSectorMap.at(iRunIndex).first <<" | ";
    for (unsigned int iSector=0; iSector<nSectors; iSector++){
      cout <<badSectorMap.at(iRunIndex).second.at(iSector) <<" ";
    }

    cout <<endl;
    
  }

  
}

//_____________________________________________
std::pair<int,double> GetBestSector(Double_t zSign, TH1D *sectorHisto){

  //Return the sector with the most tracks
  //[0-12] if zSign >= 0
  //[13-24] if zSign < 0

  Int_t minSectorID=1;
  Int_t maxSectorID=12;
  if (zSign < 0){
    minSectorID = 13;
    maxSectorID = 24;
  }

  //Loop Over the bins of the histogram and return the best sector
  Int_t highSector=0;
  Double_t highCount=-1;
  for (int iSector=minSectorID; iSector<=maxSectorID; iSector++){

    Double_t sectorContent = sectorHisto->GetBinContent(sectorHisto->FindBin(iSector));
    if (sectorContent > highCount){
      highCount = sectorContent;
      highSector = iSector;
    }

  }//End Loop Over Sectors

  return std::make_pair(highSector,highCount);;
  
}
