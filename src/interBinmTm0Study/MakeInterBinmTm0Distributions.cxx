#include <iostream>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH3D.h>
#include <TMath.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"
#include "SpectraCorrectionFunctions.h"

//The point of this study is to understand the distribution of transverse masses
//within a single transverse mass bin. By constructing the distributions and
//then determining their means and variances we know at what mTm0 the spectra
//point should be and what its horizontal errors are.

void MakeInterBinmTm0Distributions(TString inputFile, TString correctionFile, TString tofMatchEffFile, TString outputFile){

  //Get the Z-Vertex Cuts
  std::pair<double,double> zVertexCuts = GetZVertexCuts();

  //Read the input files
  DavisDstReader *davisDst = NULL;
  if (zVertexCuts.first != -999 && zVertexCuts.second != -999)
    davisDst = new DavisDstReader(inputFile,zVertexCuts.first,zVertexCuts.second);
  else
    davisDst = new DavisDstReader(inputFile);

  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;
  
  //Create the output file
  TFile *outFile = new TFile(outputFile,"RECREATE");
  
  //Create an instance of particle info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true);
  
  //Vector of Particle types that will be considered
  std::vector<int> particles;
  particles.push_back((int)PION);
  particles.push_back((int)KAON);
  particles.push_back((int)PROTON);

  std::vector<int> charges;
  charges.push_back(-1);
  charges.push_back(1);

  const unsigned int nCentralityBins = GetNCentralityBins();
  const unsigned int nParticles = particles.size();
  const unsigned int nCharges = charges.size();
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();
  std::vector<double> centralityPercents = GetCentralityPercents();
  
  //Create Histograms for the mTm0 Distributions
  std::vector < std::vector < std::vector<TH3F *> > >
    mTm0Histo(nParticles, std::vector < std::vector <TH3F *> >
	      (nCharges,std::vector <TH3F *>
	       (nCentralityBins,(TH3F *)NULL)));
  std::vector < std::vector < std::vector<TH3F *> > >
    mTm0Histo_Unweighted(nParticles, std::vector < std::vector <TH3F *> >
			 (nCharges,std::vector <TH3F*>
			  (nCentralityBins,(TH3F*)NULL)));
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	
	mTm0Histo.at(iParticle).at(iCharge).at(iCentBin) =
	  new TH3F(Form("mTm0Histo_%s_Cent%02d",
			particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
			iCentBin),
		   Form("Intra-Bin m_{T}-m_{%s} Distribution | PID: %s | Cent=[%.02g,%.02g]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});(m_{T}-m_{%s})_{Measured} - (m_{T}-m_{%s})_{Low Bin Edge} (GeV/c^{2})",
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			iCentBin == nCentralityBins-1 ? 0:centralityPercents.at(iCentBin+1),
			centralityPercents.at(iCentBin),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data()),
		   nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,
		   250,0,mTm0BinWidth);
	
	mTm0Histo_Unweighted.at(iParticle).at(iCharge).at(iCentBin) =
	  new TH3F(Form("mTm0Histo_Unweighted_%s_Cent%02d",
			particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
			iCentBin),
		   Form("Intra-Bin m_{T}-m_{%s} Distribution (Unweighted) | PID: %s | Cent=[%.02g,%.02g]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});(m_{T}-m_{%s})_{Measured} - (m_{T}-m_{%s})_{Low Bin Edge} (GeV/c^{2})",
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			iCentBin == nCentralityBins-1 ? 0:centralityPercents.at(iCentBin+1),
			centralityPercents.at(iCentBin),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data(),
			particleInfo->GetParticleSymbol(particles.at(iParticle),charges.at(iCharge)).Data()),
		   nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,
		   250,0,mTm0BinWidth);
	
	mTm0Histo.at(iParticle).at(iCharge).at(iCentBin)->Sumw2();
	mTm0Histo_Unweighted.at(iParticle).at(iCharge).at(iCentBin)->Sumw2();      

      }//End Loop Over Centrality Bins
    }//End Loop Over Charges
  }//End Loop Over Particles To create histograms
    

  //Load the Energy Loss, TPC Efficiency, and TOF Efficiency Curves from the Correction File
  TFile *corrFile = new TFile(correctionFile,"READ");
  TFile *tofEffFile = new TFile(tofMatchEffFile,"READ");
  std::vector < std::vector < std::vector < std::vector <TF1 *> > > > energyLossCurve
    (nParticles, std::vector < std::vector < std::vector <TF1 *> > >
     (nCharges, std::vector< std::vector <TF1 *> >
      (nCentralityBins, std::vector <TF1 *>
       (nRapidityBins, (TF1 *)NULL))));
  std::vector < std::vector < std::vector < std::vector <TF1 *> > > > efficiencyCurve
    (nParticles, std::vector < std::vector < std::vector <TF1 *> > >
     (nCharges,std::vector< std::vector <TF1 *> >
      (nCentralityBins, std::vector <TF1 *>
       (nRapidityBins, (TF1 *)NULL))));
  std::vector < std::vector < std::vector < std::vector <TF1 *> > > > tofEfficiencyCurve
    (nParticles, std::vector < std::vector < std::vector <TF1 *> > >
     (nCharges,std::vector< std::vector <TF1 *> >
      (nCentralityBins, std::vector <TF1 *>
       (nRapidityBins, (TF1 *)NULL))));
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
	  
	  energyLossCurve.at(iParticle).at(iCharge).at(iCentBin).at(yIndex) =
	    (TF1 *)corrFile->Get(Form("%s/EnergyLossFits/energyLossFit_%s_yIndex%d",
				      particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
				      particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
				      yIndex));

	  efficiencyCurve.at(iParticle).at(iCharge).at(iCentBin).at(yIndex) =
	    (TF1 *)corrFile->Get(Form("%s/EfficiencyFits/tpcEfficiencyFit_%s_Cent%d_yIndex%d",
				      particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
				      particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
				      iCentBin,yIndex));

	  tofEfficiencyCurve.at(iParticle).at(iCharge).at(iCentBin).at(yIndex) =
	    (TF1 *)tofEffFile->Get(Form("TofEfficiencyCurves/tofMatchingEfficiencyCurve_Cent%d_%s_%d",
					iCentBin,
					particleInfo->GetParticleName(particles.at(iParticle)).Data(),
					yIndex));

	}//End Loop Over Rapidity Bins
      }//End Loop Over the centrality bins
    }//End Loop Over Charges
  }//End Loop Over Particles

  
  //----- Loop Over the Entries in the Tree -----
  const long nEntries = davisDst->GetEntries();
  for (long iEntry = 0; iEntry < nEntries; iEntry++){

    //Get the Event entry from the DavisDst and make sure its good
    event = davisDst->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;

    //Loop Over Vertices
    for (int iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){
      
      //Get the ith vertex and check if it is good
      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
	continue;
      
      //Compute the Centrality Variable
      int centVariable = GetCentralityVariable(primaryVertex);
      
      //Get the Centrality Bin (skip if bad)
      int iCentBin = GetCentralityBin(centVariable,
				      event->GetTriggerID(&allowedTriggers),
				      primaryVertex->GetZVertex());
      if (iCentBin < 0)
	continue;

      //Loop Over the Tracks
      for (int iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){
	
	//Get the ith track and check if it is good
	track = primaryVertex->GetPrimaryTrack(iTrack);
	if (!IsGoodTrack(track))
	  continue;

	//Loop Over the Particle Species Being considered,
	//for each one compute the kinematics assuming its mass
	//and fill the required histograms
	for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

	  //Compute Kinematics and get track info
	  int particleSpecies  = particles.at(iParticle);
	  float massAssumption = particleInfo->GetParticleMass(particleSpecies);
	  float rapidity       = track->GetRapidity(massAssumption);
	  float mTm0           = track->GetmTm0(massAssumption);
	  float dEdx           = track->GetdEdx()*1000000;
	  float inverseBeta    = 1.0/track->GetTofBeta();
	  float pTotal         = track->GetPTotal();
	  float pT             = track->GetPt();
	  int   charge         = track->GetCharge();

	  //Set the Charge index
	  int iCharge(0);
	  if (charge < 0)
	    iCharge = 0;
	  else
	    iCharge = 1;
	  
	  //Make Sure the indicies are in range
	  Int_t yIndex = GetRapidityIndex(rapidity);
	  Int_t mTm0Index = GetmTm0Index(mTm0);
	  if (yIndex < 0 || yIndex > nRapidityBins)
	    continue;
	  if (mTm0Index < 1 || mTm0Index >= nmTm0Bins)
	    continue;

	  //Check that at least the TPC Efficicency Curve exists for this bin
	  if (!efficiencyCurve.at(particles.at(iParticle)).at(iCharge).at(iCentBin).at(yIndex))
	    continue;
	  
	  //Determine if the tracks is a good TOF Track
	  Bool_t isGoodTofTrack = IsGoodTofTrack(track);
	  Bool_t fillTrack = false;

	  //If the track is a good tof track then it must also be reweighted by
	  //the tof matching efficiency
	  float tofMatchingEff(1);
	  double minTofMatchFit(0.4), maxTofMatchFit(2.0);
	  	  
	  //If the tof matching efficiency curve exists and the track is a good tof track then use the tof info
	  if (tofEfficiencyCurve.at(particles.at(iParticle)).at(iCharge).at(iCentBin).at(yIndex)){

	    tofEfficiencyCurve.at(particles.at(iParticle)).at(iCharge).at(iCentBin).at(yIndex)->
	      GetRange(minTofMatchFit,maxTofMatchFit);
	    
	    if (isGoodTofTrack && mTm0 >= minTofMatchFit && mTm0 <= maxTofMatchFit){
	      
	      tofMatchingEff =
		tofEfficiencyCurve.at(particles.at(iParticle)).at(iCharge).at(iCentBin).at(yIndex)->Eval(mTm0);
	      
	      if (particleInfo->DoTPCandTOFAgreeOnThisSpecies(dEdx,inverseBeta,pTotal,particleSpecies))
		fillTrack = true;
	      
	    }
	  }
	  //Otherwise use the tpc to do pid, but only if the track
	  //has sufficienty low mTm0.
	  else if (mTm0 < minTofMatchFit){
	    if (particleInfo->GetPercentErrorTPC(dEdx,pTotal,particleSpecies) < 20)
	      fillTrack = true;
	    
	  }

	  //Fill the Histogram with the Track
	  if (fillTrack){

	    //Apply the Energy Loss Correction
	    float energyLossCorrection =
	      energyLossCurve.at(particles.at(iParticle)).at(iCharge).at(iCentBin).at(yIndex)->Eval(pT);
	    float correctedPt = pT - energyLossCorrection;
	    float correctedmTm0 = ComputemTm0(correctedPt,massAssumption);

	    //Apply the Efficiency Correction
	    float efficiencyCorrection =
	      efficiencyCurve.at(particles.at(iParticle)).at(iCharge).at(iCentBin).at(yIndex)->Eval(correctedmTm0);
	    	    
	    //Fill the Histogram with a weight given by the efficiency Corrections
	    mTm0Histo.at(iParticle).at(iCharge).at(iCentBin)->
	      Fill(rapidity, mTm0, mTm0 - GetmTm0RangeLow(mTm0Index),
		   (1.0/efficiencyCorrection)*(1.0/tofMatchingEff));

	    //Fill the Unweighted Histogram with no weights
	    mTm0Histo_Unweighted.at(iParticle).at(iCharge).at(iCentBin)->
	      Fill(rapidity, mTm0, mTm0 - GetmTm0RangeLow(mTm0Index));

	  }
	  
	}//End Loop Over Different Particle Species
	
      }//End Loop Over the Tracks
      
    }//End Loop Over Vertices in the Event

  }//End Loop Over Entries in the Tree

  //Make Directories and Save
  outFile->cd();
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){

      outFile->mkdir(Form("%s/InterBinmTm0Histos/",
			  particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data()));
      outFile->cd(Form("%s/InterBinmTm0Histos",
		       particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data()));
      
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	

	mTm0Histo.at(iParticle).at(iCharge).at(iCentBin)->
	  Write(mTm0Histo.at(iParticle).at(iCharge).at(iCentBin)->GetName(),TObject::kOverwrite);
	mTm0Histo_Unweighted.at(iParticle).at(iCharge).at(iCentBin)->
	  Write(mTm0Histo_Unweighted.at(iParticle).at(iCharge).at(iCentBin)->GetName(),TObject::kOverwrite);
	
      }//End Loop Over Centrality Bins
      outFile->cd();
    }//End Loop Over Charges
  }//End Loop Over Particles To save histograms
  
  
}
