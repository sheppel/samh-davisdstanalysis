//Use a blast wave model to simultaneously fit the pi/k/p spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TLine.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "SimFitter.h"
#include "StyleSettings.h"

bool draw = false;

//__________________________________________________________________
void simultaneousBlastWaveFit(TString spectraFileName, Int_t userCentBin = -999, Double_t userRapidity = -999){

  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int nCentBins(GetNCentralityBins());

  //Set the Charge Vector
  std::vector<int> charges;
  charges.push_back(-1);
  charges.push_back(1);

  //Set the Pid Vector
  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);
  
  const unsigned int nCharges(charges.size());
  const unsigned int nParticles(particles.size());
  
  //Open the Spectra File
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TH1F *spectraFrame = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
    gPad->SetLogy();
   
    dNdyCanvas = new TCanvas("parCanvas","parCanvas",20,700,500*nParticles,1000);
    dNdyCanvas->Divide(nParticles,2);
    for (int i=1; i<=6; i++){
      dNdyCanvas->cd(i);
      gPad->SetTopMargin(.05);
      gPad->SetRightMargin(.05);
      gPad->DrawFrame(-1.5,0,1.5,250);
    }
  }

  //Create a blast wave function. This will be copied to be the fit function
  //for each of the spectra
  TF1 *heinzBlastWaveFunc = new TF1("heinzBlastWaveFunc",BlastWaveModelFit,0.001,2,6);
  heinzBlastWaveFunc->SetNpx(100);

  TF1 *boseBlastWaveFunc = new TF1("boseBlastWaveFunc",BoseBlastWaveFit,0.001,2,8);
  boseBlastWaveFunc->SetNpx(100);

  //Create the dNdy Graphs
  std::vector<std::vector<TGraphErrors *> >dNdyGraphs
    (nCentBins, std::vector<TGraphErrors *>
     (nParticles*2,(TGraphErrors *)NULL));
  for (unsigned int iCentBin = 0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int i=0; i<nParticles*2; i++){
      dNdyGraphs.at(iCentBin).at(i) = new TGraphErrors();
      dNdyGraphs.at(iCentBin).at(i)->SetMarkerStyle(kFullCircle);
      dNdyGraphs.at(iCentBin).at(i)->SetMarkerColor(GetCentralityColor(iCentBin));
    }
  }

  //Load the Spectra
  //The most internal vector holds all the spectra of equal |y|
  std::vector<std::vector<std::vector<SpectraClass *> > > spectra
    (nCentBins,std::vector<std::vector<SpectraClass *> >
     (nRapidityBins, std::vector<SpectraClass *>
      (0,(SpectraClass *)NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > > spectraToFit
    (nCentBins,std::vector<std::vector<TGraphErrors *> >
     (nRapidityBins, std::vector<TGraphErrors *>
      (0,(TGraphErrors *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > fitFuncs 
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL)));
  std::vector<std::vector<std::vector<int> > > spectraCharge
    (nCentBins,std::vector<std::vector<int> >
     (nRapidityBins, std::vector<int>
      (0)));
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<=nRapidityBins/2; yIndex++){

      std::vector<int> yIndexVec;
      yIndexVec.push_back(yIndex);
      if ((int)yIndex != GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)))
	yIndexVec.push_back(GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)));
      
      
      for (unsigned int iY=0; iY<yIndexVec.size(); iY++){
	for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){
	  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
	    
	    const int pid = particles.at(iParticle);
	    const int charge = charges.at(iCharge);

	    TString spectraName = TString::Format("CorrectedSpectra_%s_Cent%02d_yIndex%02d_Total",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  iCentBin,yIndexVec.at(iY));
	    TString spectraClassDir = TString::Format("SpectraClass_%s",
						      particleInfo->GetParticleName(pid,charge).Data());
	    TString spectrumDir = TString::Format("CorrectedSpectra_%s",
						  particleInfo->GetParticleName(pid,charge).Data());
	    TString spectrumFitDir = TString::Format("%s_Fit",
						     spectrumDir.Data());
	    TString dNdyDir = TString::Format("dNdyGraph_%s",
					      particleInfo->GetParticleName(pid,charge).Data());

	    spectraFile->cd(spectraClassDir.Data());
	    SpectraClass *temp = (SpectraClass *)gDirectory->Get(spectraName.Data());

	    if (temp){
	      spectraCharge.at(iCentBin).at(yIndex).push_back(charge);
	      spectra.at(iCentBin).at(yIndex).push_back(temp);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraFile(spectraFile);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraClassDir(spectraClassDir);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraDir(spectrumDir);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraNamePrefix("correctedSpectra");
	      spectra.at(iCentBin).at(yIndex).back()->LoadSpectra();

	      spectraToFit.at(iCentBin).at(yIndex).push_back(spectra.at(iCentBin).at(yIndex).back()->GetTGraph());

	      if (pid == PION)
		fitFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*boseBlastWaveFunc));
	      else 
		fitFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*heinzBlastWaveFunc));

	      fitFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit0",spectraName.Data()));
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetLineColor(particleInfo->GetParticleColor(pid));
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetLineStyle(charge < 0 ? 1 : 9);
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetLineWidth(2);
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(0,Form("A_{%s}",particleInfo->GetParticleSymbol(pid).Data()));
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(1,"T_{Kin}");
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(2,"#beta_{s}");
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(3,"n");
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(4,"R");
	      fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(5,Form("m_{%s}",particleInfo->GetParticleSymbol(pid).Data()));
	      if (pid == PION){
		fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(6,"A_{Bose}");
		fitFuncs.at(iCentBin).at(yIndex).back()->SetParName(7,"T_{Bose}");
	      }
	      
	      fitFuncs.at(iCentBin).at(yIndex).back()->FixParameter(5,particleInfo->GetParticleMass(pid));
	    }

	    
	  }//End Loop Over Particles
	}//End Loop Over Charges
      }//End Loop Over Mirrored RapidityIndices
    }//End Loop Over yIndex
  }//End Loop Over CentIndex

  //Define the Fit Parameters for the Blast Wave Function
  int nNonAmpPars(7);
  enum blastWavePars {BW_TEMP,BW_SURFACEVELOCITY,BW_TRANSVELOCITYPOWER,BW_RADIALINTEGRALLIMIT,
		      BW_PIONMASS,BW_KAONMASS,BW_PROTONMASS,BW_AMP};//,
  //BW_PIONMINUSAMP,BW_PIONPLUSAMP,BW_PROTONMINUSAMP,BW_PROTONPLUSAMP};
  
  //NOW FOR THE FITS
  std::vector<double> prevFitParams;
  for (int iCentBin=nCentBins-1; iCentBin>=0; iCentBin--){

    //Skip Bins that are not requested by the user
    if (userCentBin != -999 && userCentBin >= 0 && (int)iCentBin != userCentBin)
      continue;
    
    for (int yIndex=(nRapidityBins/2); yIndex>=0; yIndex--){

      //Skip Bins that are not requested by the user
      if (userRapidity != -999 && userRapidity != GetRapidityRangeCenter(yIndex))
	continue;

      //Get the Rapidity of This bin
      Double_t rapidity = fabs(GetRapidityRangeCenter(yIndex));
      
      //Skip this bin if there are no spectra
      const unsigned int nSpectra = spectraToFit.at(iCentBin).at(yIndex).size();
      if (nSpectra == 0)
	continue;

      //Skip this bin if there is not an even number of spectra
      if (nSpectra % 2 != 0)
	continue;

      //Count the number of different species spectra
      int nPionSpectra(0);
      int nKaonSpectra(0);
      int nProtonSpectra(0);
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetPid() == PION)
	  nPionSpectra++;
	else if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetPid() == KAON)
	  nKaonSpectra++;
	else if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetPid() == PROTON)
	  nProtonSpectra++;
      }

      //Compute the Total Number of Parameters
      const int nTotalPars = nNonAmpPars + nSpectra + nPionSpectra + 2;
      
      //Define the Parameter Relationships between the individual fits and the total fit
      std::vector<std::vector<int> > parRelations(nSpectra,std::vector<int> (0,-1));
      int iPionSpectrum(nPionSpectra);
      for (unsigned int i=0; i<nSpectra; i++){

	parRelations.at(i).resize(fitFuncs.at(iCentBin).at(yIndex).at(i)->GetNpar(),0);
	parRelations.at(i).at(0) = BW_AMP+i;
	parRelations.at(i).at(1) = BW_TEMP;
	parRelations.at(i).at(2) = BW_SURFACEVELOCITY;
	parRelations.at(i).at(3) = BW_TRANSVELOCITYPOWER;
	parRelations.at(i).at(4) = BW_RADIALINTEGRALLIMIT;

	if (spectra.at(iCentBin).at(yIndex).at(i)->GetPid() == PION){
	  parRelations.at(i).at(5) = BW_PIONMASS;
	  parRelations.at(i).at(6) = nTotalPars-(iPionSpectrum)-2;
	  if (spectra.at(iCentBin).at(yIndex).at(i)->GetCharge() < 0)
	    parRelations.at(i).at(7) = nTotalPars-2;
	  else
	    parRelations.at(i).at(7) = nTotalPars-1;
	  
	  iPionSpectrum--;
	}
	else if (spectra.at(iCentBin).at(yIndex).at(i)->GetPid() == KAON){
	  parRelations.at(i).at(5) = BW_KAONMASS;
	}
	else if (spectra.at(iCentBin).at(yIndex).at(i)->GetPid() == PROTON){
	  parRelations.at(i).at(5) = BW_PROTONMASS;
	}	
      }

   
      //----Seed the Parameters of the Total Fit----
      std::vector<double> totalFitPars(nTotalPars);

      //These are the BW Specific Parameters. If previous fit results
      //exist, then use them to specify the blast wave parameters.
      if (prevFitParams.size() > 0){
	for (unsigned int iPar=0; iPar<=BW_RADIALINTEGRALLIMIT; iPar++){
	  totalFitPars.at(iPar) = prevFitParams.at(iPar);
	}
      }
      else {
	totalFitPars.at(BW_TEMP) = .13;
	totalFitPars.at(BW_SURFACEVELOCITY) = .64;
	totalFitPars.at(BW_TRANSVELOCITYPOWER) = .73;
	totalFitPars.at(BW_RADIALINTEGRALLIMIT) = 1;
      }      

      //Masses
      totalFitPars.at(BW_PIONMASS) = particleInfo->GetParticleMass(PION);
      totalFitPars.at(BW_KAONMASS) = particleInfo->GetParticleMass(KAON);
      totalFitPars.at(BW_PROTONMASS) = particleInfo->GetParticleMass(PROTON);

      //Amplitudes
      for (unsigned int i=0; i<nSpectra; i++){
	totalFitPars.at(BW_AMP+i) = 10000;
      }

      //Pion Bose Parameters
      for (int i=0; i<nPionSpectra; i++){
	totalFitPars.at(BW_AMP+nSpectra+i) = 1000; //Pion Bose Amp
      }
      totalFitPars.at(BW_AMP+nSpectra+nPionSpectra) = .1;   //PionMinus Bose Temp
      totalFitPars.at(BW_AMP+nSpectra+nPionSpectra+1) = .1; //PionPlus Bose Temp
      
      
      //Create The Sim Fitter and configure the parameters
      SimFitter blastWaveFit(spectraToFit.at(iCentBin).at(yIndex),fitFuncs.at(iCentBin).at(yIndex));
      blastWaveFit.Init(totalFitPars);
      blastWaveFit.SetPrintLevel(1);
      blastWaveFit.SetParameterRelationships(parRelations);

      //Fix the Masses
      blastWaveFit.FixParameter(BW_PIONMASS);
      blastWaveFit.FixParameter(BW_KAONMASS);
      blastWaveFit.FixParameter(BW_PROTONMASS);
      
      //Fix Parameters
      if (rapidity != 0.0)
	blastWaveFit.FixParameter(BW_TRANSVELOCITYPOWER);
      else {
	blastWaveFit.SetParLimits(BW_TRANSVELOCITYPOWER,0,1);
      }
      
      //blastWaveFit.FixParameter(BW_SURFACEVELOCITY);
      blastWaveFit.FixParameter(BW_RADIALINTEGRALLIMIT);


      //Set Parameter Limits
      blastWaveFit.SetParLimits(BW_TEMP,.08,.3);
      blastWaveFit.SetParLimits(BW_SURFACEVELOCITY,0,1);      

      for (unsigned int i=0; i<nSpectra; i++){
	blastWaveFit.SetParLimits(BW_AMP+i,0,100000000);
      }

      for (int i=0; i<nPionSpectra; i++){
	blastWaveFit.SetParLimits(BW_AMP+nSpectra+i,1,10000);
      }
      blastWaveFit.SetParLimits(BW_AMP+nSpectra+nPionSpectra,.05,.5);
      blastWaveFit.SetParLimits(BW_AMP+nSpectra+nPionSpectra+1,.05,.5);

      
      ROOT::Fit::FitResult bwFitResults = blastWaveFit.Fit();
      std::vector<TF1 *> bwFitFuncResults = blastWaveFit.GetFitFunctions();

      if (prevFitParams.size() > 0)
	prevFitParams.clear();
      prevFitParams.resize(totalFitPars.size());
      for (unsigned int iPar=0; iPar<totalFitPars.size(); iPar++)
	prevFitParams.at(iPar) = bwFitResults.Parameter(iPar);
      
      //Add the Fit to the Spectrum
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	bwFitFuncResults.at(iSpectrum)->SetName(fitFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetName());
	spectra.at(iCentBin).at(yIndex).at(iSpectrum)->AddSpectraFit(bwFitFuncResults.at(iSpectrum),NULL,0);
	//								     blastWaveFit.GetCovarianceMatrix(iSpectrum),0);
      }

      //Move the dNdy to the dNdy Graphs
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){

	int pid = spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetPid();
	int charge = spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetCharge();
	int index = pid+nParticles*(charge < 0 ? 0 : 1);
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPoint(dNdyGraphs.at(iCentBin).at(index)->GetN(),
		   GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetRapidityIndex()),
		   spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(0));
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPointError(dNdyGraphs.at(iCentBin).at(index)->GetN()-1,
			rapidityBinWidth/2.0,
			spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(0));
	
      }
      
      //DRAW IF DESIRED
      if (draw){
	canvas->cd();
	spectraFrame = canvas->DrawFrame(0.0,.0001,2,500);

	//Loop Over the Spectra Involved in the Simulatneous Fit
	for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	  canvas->cd();
	  spectraToFit.at(iCentBin).at(yIndex).at(iSpectrum)->Draw("PZ");
	  bwFitFuncResults.at(iSpectrum)->Draw("SAME");
	  bwFitFuncResults.at(iSpectrum)->Print();

	  //Draw the dNdy Distributions
	  for (unsigned int idNdy=0; idNdy<dNdyGraphs.at(iCentBin).size(); idNdy++){
	    dNdyCanvas->cd(idNdy+1);
	    dNdyGraphs.at(iCentBin).at(idNdy)->Draw("PZ");
	  }
	  
	}//End Loop Over Spectra
	
	canvas->Update();
	dNdyCanvas->Update();
	gSystem->Sleep(100);	
      }//END Draw
      
    }//End Loop Over Rapidity Bins
  }//End Loop Over Centrality Bins
  
}
