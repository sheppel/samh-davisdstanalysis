#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2F.h>
#include <TGraphErrors.h>
#include <TFitResultPtr.h>
#include <TVirtualFitter.h>
#include <TThread.h>
#include <Fit/Fitter.h>
#include <Math/WrappedMultiTF1.h>
#include <Math/WrappedTF1.h>
#include <Fit/UnBinData.h>
#include <Fit/FitResult.h>
#include <TStyle.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "utilityFunctions.h"

bool draw = false;

void FitdEdxBetaGamma(TString inputFile){

  ParticleInfo particleInfo;
  const int nParticles = particleInfo.GetNumberOfDefinedParticles();
  
  TFile *file = new TFile(inputFile,"UPDATE");
  TFile *outFile = file;
  outFile->mkdir("BichselBetaGammaGraphsFit");
  outFile->mkdir("dEdxBetaGammaFits");

  TCanvas *canvas = NULL;
  if (draw){
    gStyle->SetOptFit(1);
    canvas = new TCanvas();
    canvas->SetLogx();
    canvas->SetLogy();
    
  }

  double minFitRange(.1);
  double maxFitRange(1000);
  int npx(1000000);

  TF1 *fitFunc = new TF1("",&particleInfo,&ParticleInfo::BichselEmpiricalFitPrototype,
			 minFitRange,maxFitRange,7,"ParticleInfo","BichselEmpiricalFitPrototype");
  fitFunc->SetNpx(npx);
  fitFunc->FixParameter(0,1.2403);//
  fitFunc->SetParameter(1,0.31426);
  fitFunc->FixParameter(2,12.428); //
  fitFunc->SetParameter(3,0.41799);
  fitFunc->FixParameter(4,1.6385);//
  fitFunc->FixParameter(5,0.72059);//
  fitFunc->SetParameter(6,2.3503);

  fitFunc->SetParLimits(1,0.1,0.6);
  fitFunc->SetParLimits(3,.1,.9);
  fitFunc->SetParLimits(6,1.0,3.0);

  
  TH2F *hist = NULL;
  TGraphErrors *graph = NULL;
  for (int iParticle=0; iParticle<nParticles; iParticle++){
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Get the 2D Histogram from the File, if it doesn't exist then skip
      file->cd();
      hist = (TH2F *)file->Get(Form("BichselBetaGammaGraphs/bichselBetaGammaHisto_%s_%d",
				    particleInfo.GetParticleName(iParticle).Data(),
				    yIndex));
      if (!hist)
	continue;

      //Create the Graph. Set the points of the graph by looping over the Xbins of the 2D
      //histogram and projecting the contents to the dE/dx axis and finding the mean
      //and width of the distribution.
      graph = new TGraphErrors();
      graph->SetName(Form("bichselBetaGammaGraph_%s_%d",
			  particleInfo.GetParticleName(iParticle).Data(),
			  yIndex));
      graph->SetTitle(Form("dE/dx vs. #beta#gamma | Mass=%s | y_{%s}=[%.03g,%.03g];#beta#gamma;dE/dx (KeV/cm)",
			   particleInfo.GetParticleSymbol(iParticle).Data(),
			   particleInfo.GetParticleSymbol(iParticle).Data(),
			   GetRapidityRangeLow(yIndex),
			   GetRapidityRangeHigh(yIndex)));
      graph->SetMarkerStyle(kFullCircle);
      int nBins = hist->GetNbinsX();
      for (int iBin=1; iBin<nBins; iBin++){

	double binCenter = hist->GetXaxis()->GetBinCenter(iBin);

	TH1D *hTemp= hist->ProjectionY("",iBin,iBin);

	if (hTemp->GetEntries() < 10){
	  delete hTemp;
	  continue;
	}

	graph->SetPoint(graph->GetN(),binCenter,hTemp->GetMean());
	graph->SetPointError(graph->GetN()-1,hist->GetXaxis()->GetBinWidth(iBin)/2.0,
			     hTemp->GetRMS());

	delete hTemp;
      }

      //Skip if the graph has too few points to be fit
      if (graph->GetN() < 5){
	delete hist;
	delete graph;
	continue;
      }

      //Set the Name Of the Fit Function and seed the free parameters
      //with the values from the previous fit
      fitFunc->SetName(Form("dEdxBetaGammaFit_%s_%d",
			    particleInfo.GetParticleName(iParticle).Data(),
			    yIndex));
      cout <<"Fitting: " <<fitFunc->GetName() <<endl;
      
      fitFunc->SetParameter(1,fitFunc->GetParameter(1));
      fitFunc->SetParameter(3,fitFunc->GetParameter(3));
      fitFunc->SetParameter(6,fitFunc->GetParameter(6));

      //Fit the graph. If necessary, fit it up to 10 times or until the
      //fit converges.
      int status(-1);
      int nTries(0);
      status = graph->Fit(fitFunc,"REX0N");
      while (status != 0 && nTries < 10){
	
	status = graph->Fit(fitFunc,"REX0N");
	nTries++;
      }

      //Skip if the fit was unsuccessful
      if (status != 0){
	delete hist;
	delete graph;
	continue;
      }

      //Otherwise add the function to the graph
      graph->GetListOfFunctions()->AddFirst(new TF1(*fitFunc));

      if (draw){
	TH1F *frame = canvas->DrawFrame(.1,1.,1000,100);
	frame->SetTitle(Form("%s;%s;%s",
			      graph->GetTitle(),
			      graph->GetXaxis()->GetTitle(),
			      graph->GetYaxis()->GetTitle()));
	graph->Draw("PZ");
	canvas->Update();
	gSystem->Sleep(1000);
	delete frame;
      }

      //Save
      outFile->cd();
      outFile->cd("BichselBetaGammaGraphsFit");
      graph->Write(graph->GetName(),TObject::kOverwrite);

      outFile->cd();
      outFile->cd("dEdxBetaGammaFits");
      fitFunc->Write(fitFunc->GetName(),TObject::kOverwrite);

      
      //Clean Up
      delete hist;
      delete graph;
      
    }//End Loop Over Rapidity Indices
  }//End Loop Over Particle Species


}
